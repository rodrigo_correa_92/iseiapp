<?php

require_once realpath($_SERVER["DOCUMENT_ROOT"]) . '/php/db/DataBase.php';
	$fecha= date('Y-m-d');
	$id_grupo = $_GET['grupo'];
	$id_escuela = $_GET['escuela'];
	
	$stmt = $dbh->prepare("SELECT * FROM Asistencia WHERE id_grupo = :p1 AND fecha = :p2");
	$stmt->bindParam(':p1', $id_grupo);
	$stmt->bindParam(':p2', $fecha);
    $stmt->execute();

	if ($stmt->rowCount() > 0) {
		$asistencias = $stmt->fetchAll(PDO::FETCH_OBJ);
		echo "<h5 class='text-center'>Ya tomaste asistencia el día de hoy a este grupo</h5><br>";
		echo "<table class='table table-bordered'>
		<tr>
			<th>Nombre y apellido</th>
			<th>Asistencia</th>
		</tr>";
		
		foreach ($asistencias as $a) {
		$stmt = $dbh->prepare("SELECT * FROM Alumno WHERE id_alumno = :p1 AND activo = 1");
		$stmt->bindParam(':p1', $a-> id_alumno);
		$stmt->execute();
		$alumnos = $stmt->fetchAll(PDO::FETCH_OBJ);
			foreach ($alumnos as $b) {
				echo "<tr>
				<td class='alumno' data-id='".$a-> id_alumno."' data-grupo='".$a-> id_grupo."' data-escuela='".$a-> id_escuela."'><a href='ficha.php?alumno=".$a-> id_alumno."&escuela=".$a-> id_escuela."&grupo=".$a-> id_grupo."'>".$b-> nombre." ".$b-> apellido."</a></td>
				<td id='".$a-> id_alumno."'><p>".getAsist($a-> asistencia)."</p></td>
			 </tr>";
			}
		
	}echo "</table><div class='form-row text-center'>
                    <div class='col-12'>
                    <a class='btn btn-warning' href='editarasistencias.php?grupo=".$id_grupo."&escuela=".$id_escuela."' role='button'>Editar</a>
                    </div>
                </div>";
	$dbh=null;
	}else{
	$stmt = $dbh->prepare("SELECT * FROM Alumno WHERE id_grupo = :p1 AND activo = 1");
	$stmt->bindParam(':p1', $id_grupo);
	$stmt->execute();
	$alumnos = $stmt->fetchAll(PDO::FETCH_OBJ);
	echo "<table class='table table-bordered'>
		<tr>
			<th>Nombre y apellido</th>
			<th>Asistencia</th>
		</tr>";
	foreach ($alumnos as $a) {
		echo "<tr>
				<td class='alumno' data-id='".$a-> id_alumno."' data-grupo='".$a-> id_grupo."' data-escuela='".$a-> id_escuela."'><a href='ficha.php?alumno=".$a-> id_alumno."&escuela=".$a-> id_escuela."&grupo=".$a-> id_grupo."'>".$a-> nombre ." ". $a-> apellido ."</a></td>
				<td id='".$a-> id_alumno."' data-sel='hoa'><select name='asist' class='custom-select' data-sel='hoa'>
               <option value='1'>Presente</option>
               <option value='2' >Ausente</option>
               <option value='3'>S.A.F.</option>
           </select></td>
			 </tr>";
	}
	echo "</table><div class='form-row text-center'>
                    <div class='col-12'>
                        <button type='submit' class='btn btn-success' name='actualizar_alumno' id='guardarAsistencias'>Guardar</button>
                    </div>
                </div>";

    }
    $dbh = null;

/*function getApenom($id)
{
	require_once realpath($_SERVER["DOCUMENT_ROOT"]) . '/php/db/DataBase.php';
	$stmt = $dbh->prepare("SELECT * FROM Alumno WHERE id_alumno = :p1 AND activo = 1");
	$stmt->bindParam(':p1', $id);
	$stmt->execute();
	$alumnos = $stmt->fetchAll(PDO::FETCH_OBJ);
	foreach ($alumnos as $alumno) {
		return $alumno-> nombre . " " . $alumno-> apellido;
	}
	$dbh=null;
}*/
function getAsist($value)
{
	switch ($value) {
		case '1':
		return "Presente";
		break;
		case '2':
		return "Ausente";
		break;
		case '3':
		return "S.A.F.";
		break;
	}
}
	
		
?>