<?php session_start();
    if (isset($_SESSION['user'])) {}else{header('location: login.php');}
 ?>
<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css" integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU" crossorigin="anonymous">
    <meta name="msapplication-TileImage" content="mstile-144x144.png" />
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
 <script
  src="https://code.jquery.com/jquery-3.3.1.min.js"
  integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
  crossorigin="anonymous"></script>
    <style>
        .card{
            padding: 5px;
            margin-top: 15px;
            background-color: #232323;
        }
        </style>
    <title>ISEI App</title>
    <link rel="manifest" href="/manifest.json">
</head>
<body>
    <nav class="navbar sticky-top navbar-expand-lg navbar-dark " style="background-color: #1a4768;">
        <a class="navbar-brand" href="#">Entrenapp</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarText" aria-controls="navbarText" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarText">
            <ul class="navbar-nav mr-auto">
            </ul>
            <ul class="navbar-nav">
                <li class="nav-item"><i class="fas fa-user"></i>  <?php echo $_SESSION['nombre'] ?></li>
                <li class="nav-item"><a class="nav-link" href="logout.php"><i class="fas fa-power-off"></i>  Cerrar Sesión</a></li>
            </ul>
        </div>
    </nav>
    <div class="container">
        <div class="row">
        <?php 
            require_once realpath($_SERVER["DOCUMENT_ROOT"]) . '/php/acciones/list_escuelas.php';  
           ?>
        </div>
    </div>


<!-- Modal -->
<div class="modal fade" id="nuevaEscuela" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Nueva escuela</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        
            <div class="form-group">
                <label for="nro">Número</label><span style="color: red; display: none" id="checkNro"> Campo requerido</span>
                <input type="number" name="nro" class="form-control" id="nro">
            </div>
            <div class="form-group">
                <label for="nombre">Nombre</label><span style="color: red; display: none" id="checkNombre"> Campo requerido</span>
                <input type="text" name="nombre" class="form-control" id="nombre">
            </div>
            <div class="form-group">
                <label for="telefono">Telefono</label>
                <input type="number" name="telefono" class="form-control" id="telefono"> 
            </div>
            <div class="form-group">
                <label for="direccion">Dirección</label>
                <input type="text" name="direccion" class="form-control" id="direccion">
            </div>
        
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
        <button type="submit" class="btn btn-success" name="nueva_escuela" id="nueva_escuela">Guardar</button>
      </div>
    </div>
  </div>
</div>
<div class="modal fade" id="ModalBorrarEscuela" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-body">
        <div class='col-md-12'>
               <div class='card text-center'>
                   <div class='card-body text-center'>
                       <i class='fas fa-trash fa-7x'></i> 
                       <hr>
                       <h3 class='card-text'>¿Está seguro que desea eliminar ésta escuela?</h3>
                       <hr>
                   </div>
               </div>
           </div>
        
        
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
        <button type="submit" class="btn btn-danger" name="eliminarEscuela" id="eliminarEscuela">Eliminar</button>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="ModalEditarEscuela" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Editar escuela</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        
            <div class="form-group">
                <label for="nro">Número</label><span style="color: red; display: none" id="checkNro"> Campo requerido</span>
                <input type="number" name="nro" class="form-control" id="nroEd">
            </div>
            <div class="form-group">
                <label for="nombre">Nombre</label><span style="color: red; display: none" id="checkNombre"> Campo requerido</span>
                <input type="text" name="nombre" class="form-control" id="nombreEd">
            </div>
            <div class="form-group">
                <label for="telefono">Telefono</label>
                <input type="number" name="telefono" class="form-control" id="telefonoEd"> 
            </div>
            <div class="form-group">
                <label for="direccion">Dirección</label>
                <input type="text" name="direccion" class="form-control" id="direccionEd">
            </div>
        
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
        <button type="submit" class="btn btn-success" name="editar_escuela" id="editar_escuela">Actualizar</button>
      </div>
    </div>
  </div>
</div>

</body>
<script>
if('serviceWorker' in navigator) {
  navigator.serviceWorker
           .register('/sw.js')
           .then(function() { console.log("Service Worker Registered"); });
}
var objEscuela = [];
var errorNro = true;
var errorNombre = true;
var IdEscuela = 0;

  var IdEscuela = 0;
  var Nro = 0;
  var Nombre = 0;
  var Telefono =0;
  var Direccion =0;

$('#nueva_escuela').click(function() {
    
    if ($('#nro').val() == '') {
        $('#checkNro').show();
        errorNro = true;
    }else{
         $('#checkNro').hide();
         errorNro = false;
    }
    if ($('#nombre').val() == '') {
        $('#checkNombre').show();
        errorNombre = true;
    }else{
         $('#checkNombre').hide();
         errorNombre = false;
    }

         if (errorNro || errorNombre) {}else{
            var nro = $('#nro').val();
            var nombre = $('#nombre').val()
            var telefono = $('#telefono').val()
            var direccion = $('#direccion').val()
            var arrayDatos = {'nro':nro,'nombre':nombre, 'telefono':telefono, 'direccion':direccion};
            objEscuela.push(arrayDatos);
            var json = JSON.stringify(objEscuela);

        $('#nueva_escuela').prop('disabled', true);
        
        $.ajax({
                        url: "/php/acciones/add_school.php",
                        type: "POST",
                        data:  {'escuela' : json},
                        dataType : 'json',
                        success: function(data)
                          {
                          if (data.respuesta == false) {alert("Carga incorrecta");}else{
                          alert("Carga correcta");location.reload();}
                          },
                        error: function() 
                        {
                          alert('Disculpe, existió un problema en el envío de datos, intente nuevamente más tarde');
                        }           
                  });
       objEscuela = [];
   }
   });
$(document).on('click','#borrarEscuela',function(e){
  e.preventDefault();
  IdEscuela = $(this).attr('data-idEscuela');
});
$(document).on('click','#eliminarEscuela',function(){
  $('#eliminarEscuela').prop('disabled', true);
  var json = {'id' : IdEscuela};
  var myJSON = JSON.stringify(json); 
  $.ajax({
                        url: "/php/acciones/delete_school.php",
                        type: "POST",
                        data:  {myJSON},
                        dataType : 'json',
                        success: function(data)
                          {
                          if (data.respuesta == false) {alert("Falló la eliminación");location.reload();}else{
                          alert("Escuela eliminada correctamente");location.reload();}
                          },
                        error: function() 
                        {
                          alert('Disculpe, existió un problema en el envío de datos, intente nuevamente más tarde');
                        }           
                  });
});
$(document).on('click','#editarEscuela',function(e){
  e.preventDefault();
  IdEscuela = $(this).attr('data-idEscuela');
  Nro = $(this).attr('data-Nro');
  Nombre = $(this).attr('data-Nombre');
  Telefono = $(this).attr('data-Telefono');
  Direccion = $(this).attr('data-Direccion');

   $('#nroEd').val(Nro);
   $('#nombreEd').val(Nombre);
   $('#telefonoEd').val(Telefono);
   $('#direccionEd').val(Direccion);

});
$(document).on('click','#editar_escuela',function(){

  Nro = $('#nroEd').val();
  Nombre = $('#nombreEd').val();
  Telefono = $('#telefonoEd').val();
  Direccion = $('#direccionEd').val();

  $('#editar_escuela').prop('disabled', true);
  var json = {'id' : IdEscuela,'Nro' : Nro,'Nombre' : Nombre,'Telefono' : Telefono, 'Direccion' : Direccion};
  var myJSON = JSON.stringify(json); 
  $.ajax({
                        url: "/php/acciones/edit_school.php",
                        type: "POST",
                        data:  {myJSON},
                        dataType : 'json',
                        success: function(data)
                          {
                          if (data.respuesta == false) {alert("Falló la edición");location.reload();}else{
                          alert("Escuela editada correctamente");location.reload();}
                          },
                        error: function() 
                        {
                          alert('Disculpe, existió un problema en el envío de datos, intente nuevamente más tarde');
                        }           
                  });
});


</script>
</html>