<?php
require_once realpath($_SERVER["DOCUMENT_ROOT"]) . '/php/db/DataBase.php';

			$id_escuela = $_GET['escuela'];
			$stmt = $dbh->prepare("SELECT * FROM Grupo WHERE activo = 1 AND id_escuela = :p1");
			$params = array(':p1' => $id_escuela);
            $stmt->execute($params);
			$listgrupos=$stmt->fetchAll(PDO::FETCH_OBJ);

			foreach ($listgrupos as $e) {
				echo "<div class='col-md-6'>
                <div class='card text-center'>
                <div class='card-header'>
                        <a class='nav-link' style='color: #3d87af; float: left;' href='editargrupo.php?grupo=".$e-> id_grupo."'><i class='fas fa-edit'></i></a><a class='nav-link' style='color: red; float: left;' href='eliminargrupo.php?grupo=".$e-> id_grupo."'><i class='fas fa-trash'></i></a>
                  </div>
                    <div class='card-body'>
                        <a href='grupo.php?grupo=".$e-> id_grupo."'><i class='fas fa-school fa-7x'></i></a>  
                        <hr>
                        <h3 class='card-text'>".$e-> nombre."</h3>
                        <hr>
                        <h6 class='card-text'>Nivel: ".$e-> nivel."</h6>
                    </div>
                </div>
            </div>";}
            echo "<div class='col-md-6'>
                <div class='card text-center'>
                    <div class='card-body'>
                        <a href='#' data-toggle='modal' data-target='#nuevoGrupo'><i class='fas fa-plus-square fa-7x'></i></a>
                        <h3 class='card-text'>Nuevo Grupo</h3>
                    </div>
                </div>
            </div>";
            }
            

            


?>