<?php

session_start();
$id_prof = $_SESSION['user'];
$asistencias =json_decode($_POST['asistencia'], true);


require_once realpath($_SERVER["DOCUMENT_ROOT"]) . '/php/db/DataBase.php';
$stmt = $dbh->prepare("UPDATE Asistencia SET asistencia = :p1 WHERE id = :p2");

foreach ($asistencias as $asists) {
	$params = array(":p1" => intval($asists['asistencia']),":p2" => intval($asists['id']));
	if ($stmt->execute($params)) {
		$respuesta = array('respuesta' => true);
	}else{
		$respuesta = array('respuesta' => false);
	}
}
$dbh=null;
echo json_encode($respuesta);

?>