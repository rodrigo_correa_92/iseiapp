<?php
require_once realpath($_SERVER["DOCUMENT_ROOT"]) . '/php/db/DataBase.php';

            $id_escuela = $_GET['escuela'];

             $check = $dbh->prepare("SELECT * FROM Escuela WHERE activo = 1 AND id_escuela = :p1 AND id_profesor = :p2");
            $check->bindParam(':p1', $id_escuela);
            $check->bindParam(':p2', $_SESSION['user']);
            $check->execute();
            $check->fetchAll(PDO::FETCH_OBJ);

            if($check->rowCount() >0){
                $stmt = $dbh->prepare("SELECT * FROM Grupo WHERE activo = 1 AND id_escuela = :p1 AND id_profesor = :p2");
            $stmt->bindParam(':p1', $id_escuela);
            $stmt->bindParam(':p2', $_SESSION['user']);
            $stmt->execute();
            $listgrupos = $stmt->fetchAll(PDO::FETCH_CLASS);

            

            foreach ($listgrupos as $e) {
                $stmt2 = $dbh->prepare("SELECT * FROM Alumno WHERE activo = 1 AND id_grupo = :p1");
                $stmt2->bindParam(':p1', $e-> id_grupo);
                $stmt2->execute();
                $stmt2->fetchAll(PDO::FETCH_OBJ);
                echo "<div class='col-md-6'>
                <div class='card text-center'>
                <div class='card-header'>
                        <a href='#' class='nav-link' style='color:#3d87af; float: right;' id='editarGrupo' data-toggle='modal' data-target='#ModalEditarGrupo' data-idGrupo='".$e-> id_grupo."' data-nombre='".$e-> nombre."' data-nivel='".$e-> nivel."' data-turno='".$e-> turno."' data-grado='".$e-> grado."' data-division='".$e-> division."'><i class='fas fa-edit'></i></a><a href='#' class='nav-link' style='color:red; float: right;' id='borrarGrupo' data-toggle='modal' data-target='#ModalBorrarGrupo' data-idGrupo='".$e-> id_grupo."'borrarGrupo'><i class='fas fa-trash'></i></a>
                  </div>
                    <div class='card-body'>
                        <a href='grupo.php?grupo=".$e-> id_grupo."&escuela=".$id_escuela."'><i class='fas fa-users fa-7x'></i></a>  
                        <hr>
                        <h3 class='card-text'>".$e-> nombre."</h3>
                        <hr>
                        <h6 class='card-text'>Nivel: ".$e-> nivel."</h6>
                        <h6 class='card-text'>Turno: ".$e-> turno."</h6>
                        <h6 class='card-text'>Grado: ".$e-> grado."</h6>
                        <h6 class='card-text'>División: ".$e-> division."</h6>
                        <h6 class='card-text'>Cant. Alumnos: ".$stmt2->rowCount()."</h6>
                        
                    </div>
                </div>
            </div>";}
            echo "<div class='col-md-6'>
                <div class='card text-center'>
                    <div class='card-body'>
                        <a  href='#' data-toggle='modal' data-target='#nuevoGrupo'><i class='fas fa-plus-square fa-7x'></i></a>
                        <h3 class='card-text'>Nuevo Grupo</h3>
                    </div>
                </div>
            </div>";
            }else{
                echo "<div class='alert alert-danger' role='alert'>No tienes acceso a esta escuela</div>";
            }


            
            
?>
