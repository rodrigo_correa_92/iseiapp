<?php
require_once realpath($_SERVER["DOCUMENT_ROOT"]) . '/php/db/DataBase.php';
            

            $id_escuela = $_GET['escuela'];
            $id_grupo = $_GET['grupo'];

			$stmt = $dbh->prepare("SELECT * FROM Grupo WHERE id_escuela = :p1 AND id_grupo = :p2 AND activo = 1 AND id_profesor = :p3"); 
			$params = array(":p1"=> $id_escuela, ":p2"=> $id_grupo, ":p3"=> $_SESSION['user']);
			$stmt->execute($params);
            $grupo= $stmt->fetchAll(PDO::FETCH_OBJ);

            $stmt2 = $dbh->prepare("SELECT * FROM Alumno WHERE id_escuela = :p1 AND id_grupo = :p2 AND activo = 1"); 
            $params = array(":p1"=> $id_escuela, ":p2"=> $id_grupo);
            $stmt2->execute($params);
            
            foreach ($grupo as $en) {
                echo "<br><h1 class='text-center'>".$en-> nombre."</h1>";
            
            if ($stmt2->rowCount() > 0){
                echo "<div class='row'>
                <div class='col-md-6'>
                <div class='card text-center'>
                    <div class='card-body'>
                        <a href='alumnos.php?grupo=".$id_grupo."&escuela=".$en->id_escuela."'><i class='fas fa-search fa-7x'></i></a>
                        <h3 class='card-text'>Ver Alumnos</h3>
                    </div>
                </div>
            </div>";
            }

            echo "<div class='col-md-6'>
                <div class='card text-center'>
                    <div class='card-body'>
                        <a  href='#' data-toggle='modal' data-target='#nuevoAlumno'><i class='fas fa-user-plus fa-7x'></i></a>
                        <h3 class='card-text'>Nuevo alumno</h3>
                    </div>
                </div>
            </div>";
            if ($stmt2->rowCount() > 0){
            echo "<div class='col-md-6'>
                <div class='card text-center'>
                    <div class='card-body'>
                        <a href='asistencias.php?grupo=".$id_grupo."&escuela=".$en->id_escuela."'><i class='fas fa-book fa-7x'></i></a>
                        <h3 class='card-text'>Asistencias</h3>
                    </div>
                </div>
            </div>";

            echo "<div class='col-md-6'>
                <div class='card text-center'>
                    <div class='card-body'>
                        <a href='examenes.php?grupo=".$id_grupo."&escuela=".$en->id_escuela."'><i class='fas fa-dumbbell fa-7x'></i></a>
                        <h3 class='card-text'>Examenes</h3>
                    </div>
                </div>
            </div>";
                }
            }


?>