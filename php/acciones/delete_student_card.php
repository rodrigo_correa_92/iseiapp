<?php
require_once realpath($_SERVER["DOCUMENT_ROOT"]) . '/php/db/DataBase.php';
            $idalumno = $_GET['alumno']; 
            $stmt = $dbh->prepare("SELECT * FROM Alumno WHERE id_alumno = :p1");
            $params = array(":p1"=> $idalumno);
            $stmt->execute($params);
            $alumno= $stmt->fetchAll(PDO::FETCH_OBJ);
            
            foreach ($alumno as $en) {
               echo "<div class='col-md-12'>
               <div class='card text-center'>
                   <div class='card-body'>
                       <i class='fas fa-user-times fa-7x'></i> 
                       <hr>
                       <h3 class='card-text'>¿Estás seguro que deseas eliminar el alumno: ".$en-> nombre. " " .$en-> apellido."?</h3>
                       <hr>
                       <div class='form-row text-center'>
                        <div class='col-12'>
                        <a class='btn btn-danger' href='php/acciones/delete_student.php?alumno=".$en-> id_alumno."&escuela=". $en-> id_escuela ."&grupo=". $en-> id_grupo ."' role='button'>Eliminar</a>
                        </div>
                    </div>
                   </div>
               </div>
           </div>";
            }

?>