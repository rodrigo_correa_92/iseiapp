<?php
require_once realpath($_SERVER["DOCUMENT_ROOT"]) . '/php/db/DataBase.php';

            $id_registro = $_GET['registro']; 
			
			$stmt2 = $dbh->prepare("SELECT * FROM Notas WHERE id_registro = :p1");
			$stmt2->bindParam(':p1', $id_registro);
			$stmt2->execute();
            $notas = $stmt2->fetchAll(PDO::FETCH_OBJ);
            echo "<table class='table table-bordered'>
        <tr>
            <th>Nombre y apellido</th>
            <th>Nota</th>
        </tr>";
        foreach ($notas as $nota) {
            $alumno = new Alumno();
                echo "<tr>
                <td class='alumno' data-idAlumno='".$nota-> id_alumno."' data-id='".$nota-> id."'>".$alumno->getApeNom($nota-> id_alumno)."</td>
                <td id='".$nota-> id_alumno."'><input type='number' value='".$nota-> nota."'></td>

             </tr>";
            }
        echo "</table><div class='form-row text-center'>
                    <div class='col-12'> <button type='submit' class='btn btn-success' name='ActualizarNota' id='ActualizarNota'>Actualizar</button>
                    </div>
                </div>";