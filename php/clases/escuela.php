<?php 
	class Escuela{
            private $id_profesor;
            private $id_escuela;
            private $nro;
            private $nombre;
            private $telefono;
            private $direccion;
            public function getIdProfesor()
            {
                        return $this->id_profesor;
            }
            public function setIdProfesor($id_profesor)
            {
                        $this->id_profesor = $id_profesor;

                        return $this;
            }
            
            public function getDireccion()
            {
                        return $this->direccion;
            }
            public function setDireccion($direccion)
            {
                        $this->direccion = $direccion;

                        return $this;
            }

            public function getTelefono()
            {
                        return $this->telefono;
            }
            public function setTelefono($telefono)
            {
                        $this->telefono = $telefono;

                        return $this;
            }

            public function getNombre()
            {
                        return $this->nombre;
            }
            public function setNombre($nombre)
            {
                        $this->nombre = $nombre;

                        return $this;
            }

            public function getNro()
            {
                        return $this->nro;
            }
            public function setNro($nro)
            {
                        $this->nro = $nro;

                        return $this;
            }
            
            public function getId_escuela()
            {
                        return $this->id_escuela;
            }
            public function setId_escuela($id_escuela)
            {
                        $this->id_escuela = $id_escuela;

                        return $this;
            }


            public function agregar(){
                require_once realpath($_SERVER["DOCUMENT_ROOT"]) . '/php/db/DataBase.php';

                $stmt = $dbh->prepare("INSERT INTO Escuela (id_profesor,nro,nombre,telefono,direccion,activo) values(:p1,:p2,:p3,:p4,:p5,:p6)");
                $params = array(":p1" => $this->id_profesor,":p2" => $this->nro, ":p3" => $this->nombre, ":p4" => $this->telefono, ":p5" => $this->direccion, ":p6" => 1);
                if($stmt->execute($params)){
                    $dbh=null;
                    return true;
                }else{
                    $dbh=null;
                    return false;
                }
                
                
            }
            public function Editar()
        {
            require_once realpath($_SERVER["DOCUMENT_ROOT"]) . '/php/db/DataBase.php';
            $stmt = $dbh->prepare("UPDATE Escuela SET nro= :p1, nombre = :p2, telefono = :p3, direccion = :p4 WHERE id_escuela = :p5");
            $params = array(":p1" => $this->nro,":p2" => $this->nombre, ":p3" => $this->telefono, ":p4" => $this->direccion, ":p5" => $this->id_escuela);
            if($stmt->execute($params)){
                return true;
            }else{
                return false;
            }

        }
        public function Eliminar()
        {
            require_once realpath($_SERVER["DOCUMENT_ROOT"]) . '/php/db/DataBase.php';
            $stmt = $dbh->prepare("UPDATE Escuela SET activo=:p1 WHERE id_escuela = :p2");
            $params = array(":p1" => 0, ":p2" => $this->id_escuela);
            if($stmt->execute($params)){
                return true;
            }else{
                return false;
            }
            
        }

    }