<?php
require_once realpath($_SERVER["DOCUMENT_ROOT"]) .'/php/clases/alumno.php';
	session_start();
	$alumno =json_decode($_POST['alumno'], true);
	$id_prof = $_SESSION['user'];

    $a = new Alumno();
    $a->setId_escuela($alumno[0]['escuela']);
	$a->setNombre($alumno[0]["nombre"]);
	$a->setApellido($alumno[0]["apellido"]);
	$a->setDoc($alumno[0]["documento"]);
    $a->setFecha_nac($alumno[0]['fecha_nac']);
	$a->setGrupo($alumno[0]["grupo"]);
	$a->setGenero($alumno[0]["genero"]);
	$a->setObs($alumno[0]["obs"]);
	
	if($a->Agregar()){
		$respuesta = array('respuesta' => true);
	}else{
		$respuesta = array('respuesta' => false);
	}
	echo json_encode($respuesta);
	
?>