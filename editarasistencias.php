<?php session_start();
    if (isset($_SESSION['user'])) {}else{header('location: login.php');}
 ?>
<!DOCTYPE html>
<html lang="es">
<head>
      <?php require_once realpath($_SERVER["DOCUMENT_ROOT"]) . '/head.html';?>
    <style>
        .card{
            padding: 5px;
            margin-top: 15px;
            background-color: #232323;
        }
        </style>
    <title>ISEI App</title>
</head>
<body>
  <?php 
            require_once realpath($_SERVER["DOCUMENT_ROOT"]) . '/nav.html';  
           ?>
    <div class="container">
    	<h1 style="text-align: center;">Actualizar asistencia</h1><br>
        	<h2 style="text-align: center;"><?php echo date('d/m/y') ?></h2>	
        <?php 
            require_once realpath($_SERVER["DOCUMENT_ROOT"]) . '/php/acciones/editassist.php';  
           ?>
        </div>
    </div>
</body>
<script type="text/javascript">
    var objAlumnos = [];
    $('#actualizarAsistencias').click(function() {
        $(".alumno").each(function( index ) {
            var id_alumno = $(this).attr('data-id');
            var id = $(this).attr('data-idAsist');
            var asistencia = $("#"+id_alumno).children().children('option:selected').val();
            var arrayDatos = {'id':id,'asistencia':asistencia};
            objAlumnos.push(arrayDatos);
        });
        var json = JSON.stringify(objAlumnos);
        
        $.ajax({
                        url: "/php/acciones/updateassist.php",
                        type: "POST",
                        data:  {'asistencia' : json},
                        dataType : 'json',
                        success: function(data)
                          {
                          if (data.respuesta == false) {alert("Carga incorrecta");}else{
                          alert("Carga correcta");
                         window.location.href = 'https://rodrigocorrea.me/grupo.php?grupo=<?php echo $_GET['grupo'].'&escuela='.$_GET['escuela'];?>';}
                          },
                        error: function() 
                        {
                          alert('Disculpe, existió un problema en el envío de datos, intente nuevamente más tarde');
                        }           
                  });
        objAlumnos = [];
    });

</script>
</html>