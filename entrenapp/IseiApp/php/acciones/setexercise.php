<?php
session_start();
$id_prof = $_SESSION['user'];
$examen =json_decode($_POST['examen'], true);
$id_ejercicio = $examen['0']['id_ejercicio'];
$id_grupo = $examen['0']['grupo'];

require_once realpath($_SERVER["DOCUMENT_ROOT"]) . '/php/db/DataBase.php';
$stmt = $dbh->prepare("INSERT INTO Registroejer (id_ejercicio, id_profesor, id_grupo, fecha) values(:p0,:p1,:p2,:p3)");
$params = array(":p0" => $id_ejercicio, ":p1" => $id_prof,":p2" => $id_grupo,":p3" => date("Y-m-d"));
$stmt->execute($params);
$id_asist = $dbh->lastInsertId();

$stmt = $dbh->prepare("INSERT INTO Notas (id_registro, id_alumno, nota) values(:p0,:p1,:p2)");

foreach ($examen as $asists) {
	$params = array(":p0" => intval($id_asist),":p1" => intval($asists['alumno']), ":p2" =>intval($asists['nota']));
	if ($stmt->execute($params)) {
		$respuesta = array('respuesta' => true);
	}else{
		$respuesta = array('respuesta' => false);
	}
}
$dbh=null;
echo json_encode($respuesta);

?>