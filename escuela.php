<?php 
session_start();
    if (isset($_SESSION['user'])) {}else{header('location: login.php');} ?>
<!DOCTYPE html>
<html lang="es">
<head>
    <?php require_once realpath($_SERVER["DOCUMENT_ROOT"]) . '/head.html';?>
    <style>
        .card{
            padding: 5px;
            margin-top: 15px;
            background-color: #232323;
        }
        </style>
    <title>ISEI App</title>
</head>
<body>
  <?php 
            require_once realpath($_SERVER["DOCUMENT_ROOT"]) . '/nav.html';  
           ?>
    <div class="container">
        <div class="row">

        <?php 
            require_once realpath($_SERVER["DOCUMENT_ROOT"]) . '/php/acciones/school.php';  
           ?>
        </div>
    </div>
<!-- Modal -->
<div class="modal fade" id="nuevoGrupo" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Nuevo grupo</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        
            <input type="hidden" name="escuela" id="escuela" value=<?php echo $_GET['escuela'];?>>
            <div class="form-group">
                <label for="nombre">Nombre</label>
                <input type="text" name="nombre" class="form-control" id="nombre">
            </div>
             <div class="form-group">
                <label for="nivel">Nivel</label>
                <select class="form-control" name="nivel" id="nivel">
                    <option value="Jardín">Jardín</option>
                    <option value="Primaria">Primaria</option>
                    <option value="Secundaria">Secundaria</option>
                </select>
            </div>
            <div class="form-group">
                <label for="turno">Turno</label>
                <select class="form-control" name="turno" id="turno">
                    <option value="Mañana">Mañana</option>
                    <option value="Tarde">Tarde</option>
                </select>
            </div>
            <div class="form-group">
                <label for="grado">Grado</label>
                <select class="form-control" name="grado" id="grado">
                <optgroup label="Jardin">
                    <option value="Jardín">Jardín</option>
                </optgroup>
                <optgroup label="Primaria">
                    <option value="Primero">Primero</option>
                    <option value="Segundo">Segundo</option>
                    <option value="Tercero">Tercero</option>
                    <option value="Cuarto">Cuarto</option>
                    <option value="Quinto">Quinto</option>
                    <option value="Sexto">Sexto</option>
                    <option value="Septimo">Septimo</option>
                </optgroup>
                <optgroup label="Secundaria">
                    <option value="Primer año">Primer año</option>
                    <option value="Segundo año">Segundo año</option>
                    <option value="Tercer año">Tercer año</option>
                    <option value="Cuarto año">Cuarto año</option>
                    <option value="Quinto año">Quinto año</option>
                </optgroup>
                </select> 
            </div>
            <div class="form-group">
                <label for="division">División</label><span style="color: red; display: none" id="checkDivision"> Campo requerido</span>
                <input type="text" name="division" class="form-control" id="division">
            </div>
        
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
        <button type="submit" class="btn btn-success" name="nuevo_grupo" id="nuevo_grupo">Guardar</button>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="ModalBorrarGrupo" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-body">
        <div class='col-md-12'>
               <div class='card text-center'>
                   <div class='card-body text-center'>
                       <i class='fas fa-trash fa-7x'></i> 
                       <hr>
                       <h3 class='card-text'>¿Está seguro que desea eliminar éste grupo?</h3>
                       <hr>
                   </div>
               </div>
           </div>
        
        
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
        <button type="submit" class="btn btn-danger" name="eliminarGrupo" id="eliminarGrupo">Eliminar</button>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="ModalEditarGrupo" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Editar grupo</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        
            <div class="form-group">
                <label for="nombre">Nombre</label>
                <input type="text" name="nombre" class="form-control" id="nombreEd">
            </div>
             <div class="form-group">
                <label for="nivel">Nivel</label>
                <select class="form-control" name="nivel" id="nivelEd">
                    <option value="Jardín">Jardín</option>
                    <option value="Primaria">Primaria</option>
                    <option value="Secundaria">Secundaria</option>
                </select>
            </div>
            <div class="form-group">
                <label for="turno">Turno</label>
                <select class="form-control" name="turno" id="turnoEd">
                    <option value="Mañana">Mañana</option>
                    <option value="Tarde">Tarde</option>
                </select>
            </div>
            <div class="form-group">
                <label for="grado">Grado</label>
                <select class="form-control" name="grado" id="gradoEd">
                <optgroup label="Jardin">
                    <option value="Jardín">Jardín</option>
                </optgroup>
                <optgroup label="Primaria">
                    <option value="Primero">Primero</option>
                    <option value="Segundo">Segundo</option>
                    <option value="Tercero">Tercero</option>
                    <option value="Cuarto">Cuarto</option>
                    <option value="Quinto">Quinto</option>
                    <option value="Sexto">Sexto</option>
                    <option value="Septimo">Septimo</option>
                </optgroup>
                <optgroup label="Secundaria">
                    <option value="Primer año">Primer año</option>
                    <option value="Segundo año">Segundo año</option>
                    <option value="Tercer año">Tercer año</option>
                    <option value="Cuarto año">Cuarto año</option>
                    <option value="Quinto año">Quinto año</option>
                </optgroup>
                </select> 
            </div>
            <div class="form-group">
                <label for="division">División</label><span style="color: red; display: none" id="checkDivision"> Campo requerido</span>
                <input type="text" name="division" class="form-control" id="divisionEd">
            </div>
        
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
        <button type="submit" class="btn btn-success" name="editar_grupo" id="editar_grupo">Actualizar</button>
      </div>
    </div>
  </div>
</div>



</body>
<script>
if('serviceWorker' in navigator) {
  navigator.serviceWorker
           .register('/sw.js')
           .then(function() { console.log("Service Worker Registered"); });
}
var objGrupo = [];
var idGrupo = '';


$('#nuevo_grupo').click(function() {

 if ($('#division').val() == '') {
        $('#checkDivision').show();
        errorDiv = true;
    }else{
         $('#checkDivision').hide();
         errorDiv = false;
    }


if (errorDiv) {}else{

            var escuela = $('#escuela').val();
            var nombre = $('#nombre').val();
            var nivel = $('#nivel').val();
            var turno = $('#turno').val();
            var grado = $('#grado').val();
            var division = $('#division').val();
            var arrayDatos = {'escuela':escuela,'nombre':nombre, 'nivel':nivel, 'turno':turno, 'grado':grado, 'division':division};
            objGrupo.push(arrayDatos);
            var json = JSON.stringify(objGrupo);

        $('#nuevo_grupo').prop('disabled', true);
        
        $.ajax({
                        url: "/php/acciones/add_group.php",
                        type: "POST",
                        data:  {'grupo' : json},
                        dataType : 'json',
                        success: function(data)
                          {
                          if (data.respuesta == false) {alert("Carga incorrecta");}else{
                          alert("Carga correcta");location.reload();}
                          },
                        error: function() 
                        {
                          alert('Disculpe, existió un problema en el envío de datos, intente nuevamente más tarde');
                        }           
                  });
       objGrupo = [];
     }
    });



$(document).on('click','#borrarGrupo',function(e){
  e.preventDefault();
  IdGrupo = $(this).attr('data-idGrupo');
});
$(document).on('click','#eliminarGrupo',function(){
  $('#eliminarGrupo').prop('disabled', true);
  var json = {'id' : IdGrupo};
  var myJSON = JSON.stringify(json); 
  $.ajax({
                        url: "/php/acciones/delete_group.php",
                        type: "POST",
                        data:  {myJSON},
                        dataType : 'json',
                        success: function(data)
                          {
                          if (data.respuesta == false) {alert("Falló la eliminación");location.reload();}else{
                          alert("Grupo eliminado correctamente"); location.reload();}
                          },
                        error: function() 
                        {
                          alert('Disculpe, existió un problema en el envío de datos, intente nuevamente más tarde');
                        }           
                  });
});

var nombreEdit = '';
var nivelEdit = '';
var turnoEdit = '';
var gradoEdit = '';
var divisionEdit = '';

$(document).on('click','#editarGrupo',function(e){
  e.preventDefault();
  idGrupo = $(this).attr("data-idGrupo");
  nombreEdit = $(this).attr('data-nombre');
  nivelEdit = $(this).attr('data-nivel');
  turnoEdit = $(this).attr('data-turno');
  gradoEdit = $(this).attr('data-grado');
  divisionEdit = $(this).attr('data-division');


   $('#nombreEd').val(nombreEdit);
   $('#nivelEd').val(nivelEdit);
   $('#turnoEd').val(turnoEdit);
   $('#gradoEd').val(gradoEdit);
   $('#divisionEd').val(divisionEdit);

});
$(document).on('click','#editar_grupo',function(){

  nombreEdit = $('#nombreEd').val();
  nivelEdit = $('#nivelEd').val();
  turnoEdit = $('#turnoEd').val();
  gradoEdit = $('#gradoEd').val();
  divisionEdit = $('#divisionEd').val();

  $('#editar_grupo').prop('disabled', true);
  var json = {'id' : idGrupo, 'nombre' : nombreEdit, 'nivel' : nivelEdit, 'turno':turnoEdit ,'grado':gradoEdit,'division':divisionEdit};
  var myJSON = JSON.stringify(json); 
  $.ajax({
                        url: "/php/acciones/edit_group.php",
                        type: "POST",
                        data:  {myJSON},
                        dataType : 'json',
                        success: function(data)
                          {
                          if (data.respuesta == false) {alert("Falló la edición");location.reload();}else{
                          alert("Grupo editado correctamente");location.reload();}
                          },
                        error: function() 
                        {
                          alert('Disculpe, existió un problema en el envío de datos, intente nuevamente más tarde');
                        }           
                  });
});
</script>
</html>