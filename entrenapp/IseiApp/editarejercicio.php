<?php session_start();
    if (isset($_SESSION['user'])) {}else{header('location: login.php');}
    require_once realpath($_SERVER["DOCUMENT_ROOT"]) . '/php/clases/ejercicio.php';
 ?>
<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css" integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU" crossorigin="anonymous">
    <script src="js/app.js"></script>
    <script
  src="https://code.jquery.com/jquery-3.3.1.min.js"
  integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
  crossorigin="anonymous"></script>
    <style>
        .card{
            padding: 5px;
            margin-top: 15px;
            background-color: #232323;
        }
        </style>
    <title>ISEI App</title>
</head>
<body>
       <nav class="navbar sticky-top navbar-expand-lg navbar-dark " style="background-color: #1a4768;">
        <a class="navbar-brand" href="#">Entrenapp</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarText" aria-controls="navbarText" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarText">
            <ul class="navbar-nav mr-auto">
            </ul>
            <ul class="navbar-nav">
                <li class="nav-item"><i class="fas fa-user"></i>  <?php echo $_SESSION['nombre'] ?></li>
                <li class="nav-item"><a class="nav-link" href="logout.php"><i class="fas fa-power-off"></i>  Cerrar Sesión</a></li>
            </ul>
        </div>
    </nav>
    <div class="container">
    		
        <?php 
            require_once realpath($_SERVER["DOCUMENT_ROOT"]) . '/php/acciones/editexercise.php';  
           ?>
        </div>
    </div>
</body>
<script type="text/javascript">
    var objAlumnos = [];
    $('#guardarNota').click(function() {
        $(".alumno").each(function( index ) {
            var id_alumno = $(this).attr('data-id');
            var id_grupo = $(this).attr('data-grupo');
            var id_ejercicio = $(this).attr('data-ejercicio');
            var nota = $("#"+id_alumno).children().val();
            var arrayDatos = {'alumno':id_alumno,'nota':nota, 'id_ejercicio':id_ejercicio, 'grupo':id_grupo};
            objAlumnos.push(arrayDatos);
        });
        var json = JSON.stringify(objAlumnos);
        $('#guardarNota').prop('disabled', true);
        
        $.ajax({
                        url: "/php/acciones/setexercise.php",
                        type: "POST",
                        data:  {'examen' : json},
                        dataType : 'json',
                        success: function(data)
                          {
                          if (data.respuesta == false) {alert("Carga incorrecta");}else{
                          alert("Carga correcta");location.reload();}
                          },
                        error: function() 
                        {
                          alert('Disculpe, existió un problema en el envío de datos, intente nuevamente más tarde');
                        }           
                  });
        objAlumnos = [];
    });

</script>
</html>