<?php
require_once realpath($_SERVER["DOCUMENT_ROOT"]) . '/php/db/DataBase.php';
            $idalumno = $_GET['alumno']; 
			$stmt = $dbh->prepare("SELECT * FROM Alumno WHERE id_alumno = :p1");
			$params = array(":p1"=> $idalumno);
			$stmt->execute($params);
            $alumno= $stmt->fetchAll(PDO::FETCH_OBJ);
            
            foreach ($alumno as $en) {
                echo '<form method="POST" action="">
                <input type="hidden" name="escuela" value="'.$en-> id_escuela.'">
                <input type="hidden" name="grupo" value="'.$en-> id_grupo.'">
                <div class="form-group">
                    <label for="nombre">Nombre</label>
                    <input type="text" name="nombre" class="form-control" id="nombre" value="'. $en-> nombre .'">
                </div>
                <div class="form-group">
                    <label for="apellido">Apellido</label>
                    <input type="text" name="apellido" class="form-control" id="apellido" value="'. $en-> apellido .'">
                </div>
                <div class="form-group">
                    <label for="documento">Documento</label>
                    <input type="number" name="nro_doc" class="form-control" id="documento" value="'. $en-> doc .'"> 
                </div>
                <div class="form-group">
                    <label for="fecha_nac">Fecha de nacimiento</label>
                    <input type="date" name="fecha_nac" class="form-control" id="fecha_nac" value="'. $en-> fecha_nac .'">
                </div>
                <div class="form-row text-center">
                    <div class="col-12">
                        <button type="submit" class="btn btn-success" name="actualizar_alumno">Guardar</button>
                    </div>
                </div>
            </form>';
            }

?>