<?php 
session_start();
    if (isset($_SESSION['user'])) {
        require_once realpath($_SERVER["DOCUMENT_ROOT"]) . '/php/clases/alumno.php';
    }else{header('location: login.php');}
if (isset($_REQUEST['editar_escuela'])) {
  require_once realpath($_SERVER["DOCUMENT_ROOT"]) .'/php/acciones/edit_school.php';
}
 ?>
<!DOCTYPE html>
<html lang="es">
<head>
    <?php require_once realpath($_SERVER["DOCUMENT_ROOT"]) . '/head.html';?>
    <style>
        .card{
            padding: 5px;
            margin-top: 15px;
            background-color: #232323;
        }
        </style>
    <title>ISEI App</title>
</head>
<body>
  <?php require_once realpath($_SERVER["DOCUMENT_ROOT"]) . '/nav.html';?>
    <div class="container">
    <br>
    <h1 class='text-center'>Editar examen</h1>
        <?php 
        require_once realpath($_SERVER["DOCUMENT_ROOT"]) .'/php/acciones/edit_exam.php';
        ?>
    </div>

</body>
<script type="text/javascript">
    var objAlumnos = [];
   

function goBack() {
    window.history.go(-2);
}
    $('#ActualizarNota').click(function(event) {
        event.preventDefault()
        $(".alumno").each(function( index ) {
             var id = $(this).attr('data-id');
            var id_alumno = $(this).attr('data-idAlumno');
            var nota = $("#"+id_alumno).children().val();
            var arrayDatos = {'alumno':id_alumno,'nota':nota, "id": id};
            objAlumnos.push(arrayDatos);
        });
        var json = JSON.stringify(objAlumnos);
        $('#ActualizarNota').prop('disabled', true);
        
        $.ajax({
                        url: "/php/acciones/updateexercise.php",
                        type: "POST",
                        data:  {'examen' : json},
                        dataType : 'json',
                        success: function(data)
                          {
                          if (data.respuesta == false) {alert("Carga incorrecta");location.reload();}else{
                          alert("Carga correcta");goBack();}
                          },
                        error: function() 
                        {
                          alert('Disculpe, existió un problema en el envío de datos, intente nuevamente más tarde');
                          $('#ActualizarNota').prop('disabled', false);
                        }           
                  });
        objAlumnos = [];
    });

</script>
</html> 