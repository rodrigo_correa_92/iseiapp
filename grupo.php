<?php session_start();
    if (isset($_SESSION['user'])) {}else{header('location: login.php');} ?>
<!DOCTYPE html>
<html lang="es">
<head>
    <?php require_once realpath($_SERVER["DOCUMENT_ROOT"]) . '/head.html';?>
    <style>
        .card{
            padding: 5px;
            margin-top: 15px;
            background-color: #232323;
        }
        </style>
    <title>ISEI App</title>
</head>
<body>
  <?php 
            require_once realpath($_SERVER["DOCUMENT_ROOT"]) . '/nav.html';  
           ?>
    <div class="container">
        <?php 
            require_once realpath($_SERVER["DOCUMENT_ROOT"]) . '/php/acciones/group.php';  
           ?>
        </div>
    </div>
<!-- Modal -->
<div class="modal fade" id="nuevoAlumno" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Nuevo alumno</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <input type="hidden" name="grupo" id="grupo" value=<?php echo $_GET['grupo'];?>>
        <input type="hidden" name="escuela" id="escuela" value=<?php echo $_GET['escuela'];?>>
            <div class="form-group">
                <label for="nombre">Nombre</label>
                <input type="text" name="nombre" class="form-control" id="nombre" required>
            </div>
            <div class="form-group">
                <label for="apellido">Apellido</label>
                <input type="text" name="apellido" class="form-control" id="apellido" required>
            </div>
            <div class="form-group">
                <label for="documento">Documento</label>
                <input type="number" name="nro_doc" class="form-control" id="documento" required> 
            </div>
            <div class="form-group">
                <label for="fecha_nac">Fecha de nacimiento</label>
                <input type="date" name="fecha_nac" class="form-control" id="fecha_nac" required>
            </div>
            <div class="form-group">
                <label for="fecha_nac">Género</label>
                <select name="genero" id="genero" class="form-control">
                  <option value="1">Masculino</option>
                  <option value="2">Femenino</option>
                </select>
            </div>
            <div class="form-group">
              <label for="obs">Observaciones</label>
              <textarea class="form-control" id="obs" rows="4"></textarea>
            </div>
        
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
        <button type="submit" class="btn btn-success" name="nuevo_alumno" id="nuevo_alumno">Guardar</button>
      </div>
    </div>
  </div>
</div>
</body>
<script>
if('serviceWorker' in navigator) {
  navigator.serviceWorker
           .register('/sw.js')
           .then(function() { console.log("Service Worker Registered"); });
}
var objAlumno = [];
$('#nuevo_alumno').click(function() {
            var escuela = $('#escuela').val();
            var grupo = $('#grupo').val();
            var nombre = $('#nombre').val();
            var apellido = $('#apellido').val();
            var documento = $('#documento').val();
            var fecha_nac = $('#fecha_nac').val();
            var obs = $('#obs').val();
            if (nombre == '' || apellido == '' || documento == '' || fecha_nac == '' ) {
              alert("completa todos los campos");
            }else{var genero = $('#genero option:selected').val();
            var arrayDatos = {'grupo':grupo,'escuela':escuela,'nombre':nombre, 'apellido':apellido, 'documento':documento, 'fecha_nac':fecha_nac, 'genero':genero ,"obs":obs};
            objAlumno.push(arrayDatos);
            var json = JSON.stringify(objAlumno);

        $('#nuevo_alumno').prop('disabled', true);
        
        $.ajax({
                        url: "/php/acciones/add_student.php",
                        type: "POST",
                        data:  {'alumno' : json},
                        dataType : 'json',
                        success: function(data)
                          {
                          if (data.respuesta == false) {alert("Carga incorrecta"); $('#nuevo_alumno').prop('disabled', false);}else{
                          alert("Carga correcta");location.reload();}
                          },
                        error: function() 
                        {
                          alert('Disculpe, existió un problema en el envío de datos, intente nuevamente más tarde');
                        }           
                  });
       objAlumno = [];}
            
    });
</script>
</html>