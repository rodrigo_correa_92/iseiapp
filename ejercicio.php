<?php session_start();
    if (isset($_SESSION['user'])) {}else{header('location: login.php');}
    require_once realpath($_SERVER["DOCUMENT_ROOT"]) . '/php/clases/ejercicio.php';
 ?>
<!DOCTYPE html>
<html lang="es">
<head>
    <?php require_once realpath($_SERVER["DOCUMENT_ROOT"]) . '/head.html';?>
    <style>
        .card{
            padding: 5px;
            margin-top: 15px;
            background-color: #232323;
        }
        </style>
    <title>ISEI App</title>
</head>
<body>
  <?php 
            require_once realpath($_SERVER["DOCUMENT_ROOT"]) . '/nav.html';  
           ?>
    <div class="container">
    		
        <?php 
            require_once realpath($_SERVER["DOCUMENT_ROOT"]) . '/php/acciones/exercise.php';  
           ?>
        </div>
    </div>
</body>
<script type="text/javascript">
    var objAlumnos = [];
    $('#guardarNota').click(function() {
        $(".alumno").each(function( index ) {
            var id_alumno = $(this).attr('data-id');
            var id_grupo = $(this).attr('data-grupo');
            var id_ejercicio = $(this).attr('data-ejercicio');
            var nota = $("#"+id_alumno).children().val();
            var arrayDatos = {'alumno':id_alumno,'nota':nota, 'id_ejercicio':id_ejercicio, 'grupo':id_grupo};
            objAlumnos.push(arrayDatos);
        });
        var json = JSON.stringify(objAlumnos);
        $('#guardarNota').prop('disabled', true);
        
        $.ajax({
                        url: "/php/acciones/setexercise.php",
                        type: "POST",
                        data:  {'examen' : json},
                        dataType : 'json',
                        success: function(data)
                          {
                          if (data.respuesta == false) {alert("Carga incorrecta");}else{
                          alert("Carga correcta");location.reload();}
                          },
                        error: function() 
                        {
                          alert('Disculpe, existió un problema en el envío de datos, intente nuevamente más tarde');
                        }           
                  });
        objAlumnos = [];
    });

</script>
</html>