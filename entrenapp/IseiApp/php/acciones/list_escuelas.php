<?php
session_start();
require_once realpath($_SERVER["DOCUMENT_ROOT"]) . '/php/db/DataBase.php';


			$id_prof = $_SESSION['user'];
			$stmt = $dbh->prepare("SELECT * FROM Escuela WHERE activo = 1 AND id_profesor = :p1");
            $stmt->bindParam(':p1', $id_prof);
			$stmt->execute();
			$escuelas = $stmt->fetchAll(PDO::FETCH_CLASS);

			foreach ($escuelas as $e) {
				echo "<div class='col-md-6'>
                <div class='card text-center'>
                <div class='card-header'>
                        <a class='nav-link' style='color: #3d87af; float: right;' id='editarEscuela' data-toggle='modal' data-target='#ModalEditarEscuela' data-idEscuela='".$e-> id_escuela."' data-Nombre='".$e-> nombre."' data-Nro='".$e-> nro."' data-Direccion='".$e-> direccion."' data-Telefono='".$e-> telefono."'><i class='fas fa-edit'></i></a><a href='#' class='nav-link' style='color:red; float: right;' id='borrarEscuela' data-toggle='modal' data-target='#ModalBorrarEscuela' data-idEscuela='".$e-> id_escuela."'><i class='fas fa-trash'></i></a>
                  </div>
                    <div class='card-body'>
                        <a href='escuela.php?escuela=".$e-> id_escuela."'><i class='fas fa-school fa-7x'></i></a>  
                        <hr>
                        <h3 class='card-text'>".$e-> nombre."</h3>
                        <hr>
                        <h6 class='card-text'>Nro: ".$e-> nro."</h6>
                        <h6 class='card-text'>Dirección: ".$e-> direccion."</h6>
                        <h6 class='card-text'>Telefono: ".$e-> telefono."</h6>
                    </div>
                </div>
            </div>";
            }
            echo "<div class='col-md-6'>
                <div class='card text-center'>
                    <div class='card-body'>
                        <a href='#' data-toggle='modal' data-target='#nuevaEscuela'><i class='fas fa-plus-square fa-7x'></i></a>
                        <h3 class='card-text'>Nueva Escuela</h3>
                    </div>
                </div>
            </div>";

            


?>