<?php
require_once realpath($_SERVER["DOCUMENT_ROOT"]) . '/php/db/DataBase.php';
            $idescuela = $_GET['escuela']; 
			
			$stmt = $dbh->prepare("SELECT * FROM Escuela WHERE id_escuela = :p1");
			$stmt->bindParam(':p1', $idescuela);
			$stmt->execute();
            $escuela = $stmt->fetchAll(PDO::FETCH_OBJ);
            
            foreach ($escuela as $en) {
                echo "<form method='POST' action=''>
                    <div class='form-group'>
                        <label for='nro'>Número</label>
                        <input type='number' name='nro' class='form-control' id='nro' value='". $en-> nro ."'>
                    </div>
                    <div class='form-group'>
                        <label for='nombre'>Nombre</label>
                        <input type='text' name='nombre' class='form-control' id='nombre' value='". $en-> nombre ."'>
                    </div>
                    <div class='form-group'>
                        <label for='telefono'>Telefono</label>
                        <input type='number' name='telefono' class='form-control' id='telefono' value='". $en-> telefono ."'> 
                    </div>
                    <div class='form-group'>
                        <label for='direccion'>Dirección</label>
                        <input type='text' name='direccion' class='form-control' id='direccion' value='". $en-> direccion ."'>
                    </div>
                    <div class='form-row text-center'>
                        <div class='col-12'>
                            <button type='submit' class='btn btn-success' name='editar_escuela'>Guardar</button>
                        </div>
                    </div>
                </form>";
            }
?>