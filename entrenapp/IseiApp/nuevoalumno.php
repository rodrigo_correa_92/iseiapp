<?php 
session_start();
    if (isset($_SESSION['user'])) {}else{header('location: login.php');}
if (isset($_REQUEST['nuevo_alumno'])) {
  require_once realpath($_SERVER["DOCUMENT_ROOT"]) .'/php/acciones/add_student.php';
}
 ?>
<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css" integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU" crossorigin="anonymous">
    <script src="js/app.js"></script>
    <style>
        .card{
            padding: 5px;
            margin-top: 15px;
            background-color: #232323;
        }
        </style>
    <title>ISEI App</title>
</head>
<body>
    <nav class="navbar sticky-top navbar-expand-lg navbar-dark " style="background-color: #1a4768;">
        <a class="navbar-brand" href="grupo.php?grupo= <?php echo $_GET['grupo'].'&escuela='.$_GET['escuela'];?> "><i class='fas fa-undo-alt'></i> Grupo</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarText" aria-controls="navbarText" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarText">
            <ul class="navbar-nav mr-auto nav-tabs">
            
            </ul>
            <ul class="navbar-nav">
                <li class="nav-item"><a class="nav-link" href="#"><i class="fas fa-user"></i>  Profesor</a></li>
            </ul>
        </div>
    </nav>
    <div class="container">
    <h1 class='text-center'>Nuevo alumno</h1>
        <form method="POST" action="">
            <?php
                require_once realpath($_SERVER["DOCUMENT_ROOT"]) . '/php/db/DataBase.php';
                
                $id_escuela = $_GET['escuela'];
                $id_grupo = $_GET['grupo'];
               
                $stmt = $dbh->prepare("SELECT * FROM Grupo WHERE id_escuela = :p1 AND id_grupo = :p2"); 
                $params = array(":p1"=> $id_escuela, ":p2"=> $id_grupo);
                $stmt->execute($params);
                $grupo= $stmt->fetch(PDO::FETCH_ASSOC);
                

                    echo "<input type='hidden' name='grupo' value='". $grupo['id_grupo'] ."'>
                    <input type='hidden' name='escuela' value='". $grupo['id_escuela'] ."'>";

                $dbh=null;

            ?>
            
            <div class="form-group">
                <label for="nombre">Nombre</label>
                <input type="text" name="nombre" class="form-control" id="nombre" required>
            </div>
            <div class="form-group">
                <label for="apellido">Apellido</label>
                <input type="text" name="apellido" class="form-control" id="apellido" required>
            </div>
            <div class="form-group">
                <label for="documento">Documento</label>
                <input type="number" name="nro_doc" class="form-control" id="documento" required> 
            </div>
            <div class="form-group">
                <label for="fecha_nac">Fecha de nacimiento</label>
                <input type="date" name="fecha_nac" class="form-control" id="fecha_nac" required>
            </div>
            <div class="form-row text-center">
                <div class="col-12">
                    <button type="submit" class="btn btn-success" name="nuevo_alumno">Guardar</button>
                </div>
            </div>
        </form>
    </div>
</body>
</html> 