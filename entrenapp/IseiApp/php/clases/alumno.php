<?php 

	class Alumno{

        private $id;
        private $id_escuela;
		private $nombre;
		private $apellido;
		private $doc;
		private $fecha_nac;
        private $grupo;
        private $genero;

        public function getId()
        {
                return $this->id;
        }
        public function setId($id)
        {
                $this->id = $id;

                return $this;
        }

        public function getId_escuela()
        {
                return $this->id_escuela;
        }
        public function setId_escuela($id_escuela)
        {
                $this->id_escuela = $id_escuela;

                return $this;
        }

        public function getNombre()
        {
                return $this->nombre;
        }
        public function setNombre($nombre)
        {
                $this->nombre = $nombre;

                return $this;
        }

        public function getApellido()
        {
                return $this->apellido;
        }
        public function setApellido($apellido)
        {
                $this->apellido = $apellido;

                return $this;
        }

        public function getDoc()
        {
                return $this->doc;
        }
        public function setDoc($doc)
        {
                $this->doc = $doc;

                return $this;
        }

        public function getFecha_nac()
        {
                return $this->fecha_nac;
        }
        public function setFecha_nac($fecha_nac)
        {
                $this->fecha_nac = $fecha_nac;

                return $this;
        }
        
        public function getNivel()
        {
                return $this->nivel;
        }
        public function setNivel($nivel)
        {
                $this->nivel = $nivel;

                return $this;
        }

        public function getTurno()
        {
                return $this->turno;
        }
        public function setTurno($turno)
        {
                $this->turno = $turno;

                return $this;
        }

        public function getGrado()
        {
                return $this->grado;
        }
        public function setGrado($grado)
        {
                $this->grado = $grado;

                return $this;
        }

        public function getDivision()
        {
                return $this->division;
        }

        public function setDivision($division)
        {
                $this->division = $division;

                return $this;
        }

        public function getGrupo()
        {
            return $this->grupo;
        }

        public function setGrupo($grupo)
        {
            $this->grupo = $grupo;

            return $this;
        }
        public function getGenero()
        {
            return $this->genero;
        }

        public function setGenero($genero)
        {
            $this->genero = $genero;

            return $this;
        }

        public function Agregar()

        {
            require_once realpath($_SERVER["DOCUMENT_ROOT"]) . '/php/db/DataBase.php';
            $stmt = $dbh->prepare("INSERT INTO Alumno (id_escuela,nombre,apellido,doc,fecha_nac,genero,id_grupo,activo) values(:p1,:p2,:p3,:p4,:p5,:p6,:p7,:p8)");
            $params = array(":p1" => $this->id_escuela,":p2" => $this->nombre, ":p3" => $this->apellido, ":p4" => $this->doc, ":p5" => $this->fecha_nac, ":p6" => $this->genero,":p7" => $this->grupo, ":p8" => 1);
           if($stmt->execute($params)){
                    return true;
                }else{
                    return false;
                }
        }
        public function Editar()
        {
            require_once realpath($_SERVER["DOCUMENT_ROOT"]) . '/php/db/DataBase.php';
            $stmt = $dbh->prepare("UPDATE Alumno SET nombre=:p1, apellido = :p2, doc = :p3, fecha_nac = :p4 WHERE id_alumno = :p9");
            $params = array(":p1" => $this->nombre,":p2" => $this->apellido, ":p3" => $this->doc, ":p4" => $this->fecha_nac, ":p9" => $this->id);
            $stmt->execute($params);
            $dbh=null;
            header("location:../../grupo.php?grupo=".$this->grupo."&escuela=".$this->id_escuela);
        }
        public function Eliminar()
        {
            require_once realpath($_SERVER["DOCUMENT_ROOT"]) . '/php/db/DataBase.php';
            $stmt = $dbh->prepare("UPDATE Alumno SET activo=:p1 WHERE id_alumno = :p2");
            $params = array(":p1" => 0, ":p2" => $this->id);
            $stmt->execute($params);
            $dbh=null;
            header("location:../../grupo.php?grupo=".$this->grupo."&escuela=".$this->id_escuela);
        }
        public function getGenero($id)
        {
            require_once realpath($_SERVER["DOCUMENT_ROOT"]) . '/php/db/DataBase.php';
            $stmt = $dbh->prepare("SELECT * FROM Alumno WHERE id_alumno = :p1");
            $params = array(":p1" => $id);
            $stmt->execute($params);
            $alumno= $stmt->fetch(PDO::FETCH_OBJ);

            return $alumno-> genero;

        }

    
}



 ?>