<?php session_start();
    if (isset($_SESSION['user'])) {}else{header('location: login.php');}
 ?>
<!DOCTYPE html>
<html lang="es">
<head>
    <?php require_once realpath($_SERVER["DOCUMENT_ROOT"]) . '/head.html';?>
    <style>
        .card{
            padding: 5px;
            margin-top: 15px;
            background-color: #232323;
        }
         #cover-spin {
    position:fixed;
    width:100%;
    left:0;right:0;top:0;bottom:0;
    background-color: rgba(255,255,255,0.7);
    z-index:9999;
}

@-webkit-keyframes spin {
  from {-webkit-transform:rotate(0deg);}
  to {-webkit-transform:rotate(360deg);}
}

@keyframes spin {
  from {transform:rotate(0deg);}
  to {transform:rotate(360deg);}
}

#cover-spin::after {
    content:'';
    display:block;
    position:absolute;
    left:48%;top:40%;
    width:40px;height:40px;
    border-style:solid;
    border-color:black;
    border-top-color:transparent;
    border-width: 4px;
    border-radius:50%;
    -webkit-animation: spin .8s linear infinite;
    animation: spin .8s linear infinite;
}
        </style>
    <title>ISEI App</title>
</head>
<body>
    <div id="cover-spin" style="display: none;"></div> 
  <?php 
            require_once realpath($_SERVER["DOCUMENT_ROOT"]) . '/nav.html';  
           ?>
    <div class="container">
    	<h1 style="text-align: center;">Tomar asistencia</h1><br>
        	<h2 style="text-align: center;"><?php echo date('d/m/y') ?></h2><br>	
        <?php 
            require_once realpath($_SERVER["DOCUMENT_ROOT"]) . '/php/acciones/takeassist.php';  
           ?>
        </div>
    </div>
</body>
<script type="text/javascript">
    var objAlumnos = [];
    $('#guardarAsistencias').click(function() {
        $(".alumno").each(function( index ) {
            var id_alumno = $(this).attr('data-id');
            var id_grupo = $(this).attr('data-grupo');
            var id_escuela = $(this).attr('data-escuela');
            var asistencia = $("#"+id_alumno).children().children('option:selected').val();
            var arrayDatos = {'alumno':id_alumno,'asistencia':asistencia, 'escuela':id_escuela, 'grupo':id_grupo};
            objAlumnos.push(arrayDatos);
        });
        var json = JSON.stringify(objAlumnos);
        $('#guardarAsistencias').prop('disabled', true);
        $('#cover-spin').show();
        
        $.ajax({
                        url: "/php/acciones/setassist.php",
                        type: "POST",
                        data:  {'asistencia' : json},
                        dataType : 'json',
                        success: function(data)
                          {
                          if (data.respuesta == false) {alert("Carga incorrecta");}else{$('#cover-spin').hide();
                          alert("Carga correcta");location.reload();}
                          },
                        error: function() 
                        {
                          alert('Disculpe, existió un problema en el envío de datos, intente nuevamente más tarde');
                        }           
                  });
        objAlumnos = [];
    });

</script>
</html>