<?php 
session_start();
    if (isset($_SESSION['user'])) {}else{header('location: login.php');}
if (isset($_REQUEST['nuevo_alumno'])) {
  require_once realpath($_SERVER["DOCUMENT_ROOT"]) .'/php/acciones/add_student.php';
}
 ?>
<!DOCTYPE html>
<html lang="es">
<head>
    <?php require_once realpath($_SERVER["DOCUMENT_ROOT"]) . '/head.html';?>
    <style>
        .card{
            padding: 5px;
            margin-top: 15px;
            background-color: #232323;
        }
        </style>
    <title>ISEI App</title>
</head>
<body>
       <nav class="navbar sticky-top navbar-expand-lg navbar-dark " style="background-color: #1a4768;">
        <a class="navbar-brand" href="#">Entrenapp</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarText" aria-controls="navbarText" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarText">
            <ul class="navbar-nav mr-auto">
            </ul>
            <ul class="navbar-nav">
                <li class="nav-item"><p><i class="fas fa-user"></i>  <?php echo $_SESSION['nombre'] ?></p></li>
                <li class="nav-item"><a class="nav-link" href="logout.php"><i class="fas fa-power-off"></i>  Cerrar Sesión</a></li>
            </ul>
        </div>
    </nav>
    <div class="container">
    <h1 class='text-center'>Nuevo alumno</h1>
        <form method="POST" action="">
            <?php
                require_once realpath($_SERVER["DOCUMENT_ROOT"]) . '/php/db/DataBase.php';
                
                $id_escuela = $_GET['escuela'];
                $id_grupo = $_GET['grupo'];
               
                $stmt = $dbh->prepare("SELECT * FROM Grupo WHERE id_escuela = :p1 AND id_grupo = :p2"); 
                $params = array(":p1"=> $id_escuela, ":p2"=> $id_grupo);
                $stmt->execute($params);
                $grupo= $stmt->fetch(PDO::FETCH_ASSOC);
                

                    echo "<input type='hidden' name='grupo' value='". $grupo['id_grupo'] ."'>
                    <input type='hidden' name='escuela' value='". $grupo['id_escuela'] ."'>";

                $dbh=null;

            ?>
            
            <div class="form-group">
                <label for="nombre">Nombre</label>
                <input type="text" name="nombre" class="form-control" id="nombre" required>
            </div>
            <div class="form-group">
                <label for="apellido">Apellido</label>
                <input type="text" name="apellido" class="form-control" id="apellido" required>
            </div>
            <div class="form-group">
                <label for="documento">Documento</label>
                <input type="number" name="nro_doc" class="form-control" id="documento" required> 
            </div>
            <div class="form-group">
                <label for="fecha_nac">Fecha de nacimiento</label>
                <input type="date" name="fecha_nac" class="form-control" id="fecha_nac" required>
            </div>
            <div class="form-row text-center">
                <div class="col-12">
                    <button type="submit" class="btn btn-success" name="nuevo_alumno">Guardar</button>
                </div>
            </div>
        </form>
    </div>
</body>
</html> 