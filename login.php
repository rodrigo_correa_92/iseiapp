<!DOCTYPE html>
<html lang="es">
<head>
    <?php require_once realpath($_SERVER["DOCUMENT_ROOT"]) . '/head.html';?>
    <style>
        .card{
            padding: 5px;
            margin-top: 15px;
            background-color: #232323;
        }
        </style>
    <title>ISEI App</title>
</head>

  <body>

    <div class="container">

      <form class="form-signin" method="POST" action="/php/acciones/checklogin.php">
        <hr><h2 class="form-signin-heading" style="text-align: center">Entrenapp</h2><hr>
        <label for="inputEmail" class="sr-only">Email</label>
        <input type="email" id="email" name="email" class="form-control" placeholder="Email" required autofocus>
        <label for="inputPassword" class="sr-only">Contraseña</label>
        <input type="password" id="pass" name="pass" class="form-control" placeholder="Contraseña" required>
        <hr>
        <button class="btn btn-lg btn-primary btn-block" type="submit">Acceder</button>
      </form>

    </div> 
  </body>
</html>