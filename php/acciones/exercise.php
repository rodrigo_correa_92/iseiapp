<?php

require_once realpath($_SERVER["DOCUMENT_ROOT"]) . '/php/db/DataBase.php';
	$fecha= date('Y-m-d');
	$id_grupo = $_GET['grupo'];
	$id_escuela = $_GET['escuela'];
	$id_ejercicio = $_GET['ejercicio'];

	$nombreEjer = $dbh->prepare("SELECT * FROM Ejercicio WHERE id_ejercicio = :p1");
	$nombreEjer->bindParam(':p1', $id_ejercicio);
    $nombreEjer->execute();
    $FEt = $nombreEjer->fetch(PDO::FETCH_OBJ);
    
	
	$stmt = $dbh->prepare("SELECT * FROM Registroejer WHERE id_grupo = :p1 AND fecha = :p2 AND id_ejercicio = :p3");
	$stmt->bindParam(':p1', $id_grupo);
	$stmt->bindParam(':p2', $fecha);
	$stmt->bindParam(':p3', $id_ejercicio);
    $stmt->execute();

    $stmt2 = $dbh->prepare("SELECT * FROM Asistencia WHERE id_grupo = :p1 AND fecha = :p2");
	$stmt2->bindParam(':p1', $id_grupo);
	$stmt2->bindParam(':p2', $fecha);
    $stmt2->execute();

	if($stmt->rowCount() > 0){
		$id_registro = $stmt->fetch(PDO::FETCH_OBJ);
		$stmt = $dbh->prepare("SELECT * FROM Notas WHERE id_registro = :p1");
		$stmt->bindParam(':p1', $id_registro-> id_registro);
	    $stmt->execute();
		echo "<h5 class='text-center'>Ya tomaste este examen el día de hoy a este grupo</h5><br>";
		echo "<table class='table table-bordered'>
		<tr>
			<th>Nombre y apellido</th>
			<th>Nota</th>
		</tr>";
		$notas = $stmt->fetchAll(PDO::FETCH_OBJ);
		foreach ($notas as $a) {
		$stmt = $dbh->prepare("SELECT * FROM Alumno WHERE id_alumno = :p1 AND activo = 1");
		$stmt->bindParam(':p1', $a-> id_alumno);
		$stmt->execute();
		$alumnos = $stmt->fetchAll(PDO::FETCH_OBJ);
			foreach ($alumnos as $b) {
				 $ejer = new Ejercicio();
				echo "<tr>
				<td class='alumno' data-id='".$a-> id_alumno."' data-grupo='".$a-> id_grupo."' data-escuela='".$a-> id_escuela."'><a href='ficha.php?alumno=".$a-> id_alumno."&escuela=".$a-> id_escuela."&grupo=".$a-> id_grupo."'>".$b-> nombre." ".$b-> apellido."</a></td>
				<td id='".$a-> id_alumno."'><p>".$a-> nota." (".$ejer->getCalificacion($id_registro-> id_ejercicio,$a-> nota).")</p></td>

			 </tr>";
			}
		
	}echo "</table>";
	}elseif ($stmt2->rowCount() > 0) {
	$stmt = $dbh->prepare("SELECT * FROM Alumno WHERE id_grupo = :p1 AND activo = 1");
	$stmt->bindParam(':p1', $id_grupo);
	$stmt->execute();
	$alumnos = $stmt->fetchAll(PDO::FETCH_OBJ);


	echo "<h1 style='text-align: center;'>".$FEt-> nombre."</h1><br><h2 style='text-align: center;'><?php echo date('d/m/y') ?></h2><br><table class='table table-bordered'>
		<tr>
			<th>Nombre y apellido</th>
			<th>Nota</th>
		</tr>";
	foreach ($alumnos as $a) {
		$ejer = new Ejercicio();
		if ($ejer->checkAsist($fecha, $a-> id_alumno)) {
			echo "<tr>
				<td class='alumno' data-id='".$a-> id_alumno."' data-grupo='".$a-> id_grupo."' data-ejercicio='".$id_ejercicio."'><a href='ficha.php?alumno=".$a-> id_alumno."&escuela=".$a-> id_escuela."&grupo=".$a-> id_grupo."'>".$a-> nombre ." ". $a-> apellido ."</a></td>
				<td id='".$a-> id_alumno."' data-sel='hoa'><input type='number'></td>
			 </tr>";
		}else{
			echo "<tr>
				<td class='alumno' data-id='".$a-> id_alumno."' data-grupo='".$a-> id_grupo."' data-ejercicio='".$id_ejercicio."'><a href='ficha.php?alumno=".$a-> id_alumno."&escuela=".$a-> id_escuela."&grupo=".$a-> id_grupo."'>".$a-> nombre ." ". $a-> apellido ." (No disponible)</a></td>
				<td id='".$a-> id_alumno."' data-sel='hoa'><input type='number' value='0' disabled></td>
			 </tr>";
		}
		
	}
	echo "</table><div class='form-row text-center'>
                    <div class='col-12'>
                        <button type='submit' class='btn btn-success' name='actualizar_alumno' id='guardarNota'>Guardar</button>
                    </div>
                </div>";
	$dbh=null;
	}else{
		echo "<div class='alert alert-danger' role='alert'>
  Necesitas tomar asistencia para tomar un examen
</div>";
    }

/*function getApenom($id)
{
	require_once realpath($_SERVER["DOCUMENT_ROOT"]) . '/php/db/DataBase.php';
	$stmt = $dbh->prepare("SELECT * FROM Alumno WHERE id_alumno = :p1 AND activo = 1");
	$stmt->bindParam(':p1', $id);
	$stmt->execute();
	$alumnos = $stmt->fetchAll(PDO::FETCH_OBJ);
	foreach ($alumnos as $alumno) {
		return $alumno-> nombre . " " . $alumno-> apellido;
	}
	$dbh=null;
}*/
		
?>