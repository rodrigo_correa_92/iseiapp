<?php
require_once realpath($_SERVER["DOCUMENT_ROOT"]) .'/php/clases/escuela.php';
	session_start();
	$escuela =json_decode($_POST['escuela'], true);
	$id_prof = $_SESSION['user'];

	$a = new Escuela();
	$a->setIdProfesor($id_prof);
	$a->setNro(intval($escuela[0]['nro']));
	$a->setNombre($escuela[0]['nombre']);
	$a->setTelefono(intval($escuela[0]['telefono']));
    $a->setDireccion($escuela[0]['direccion']);
	if($a->Agregar()){
		$respuesta = array('respuesta' => true);
	}else{
		$respuesta = array('respuesta' => false);
	}
	echo json_encode($respuesta);
?>
