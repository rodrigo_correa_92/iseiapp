<?php
require_once realpath($_SERVER["DOCUMENT_ROOT"]) . '/php/db/DataBase.php';
			
	$id_alumno = $_GET['alumno'];
	$stmt = $dbh->prepare("SELECT * FROM Alumno WHERE id_alumno = :p1");
	$param = array(":p1"=> $id_alumno);
	$stmt->execute($param);
    $fichaAlumno= $stmt->fetchAll(PDO::FETCH_OBJ);
    
	echo "<br><h2 class='text-center'>Ficha Completa</h2><table style='table-layout: fixed;' class='table table-bordered'>";
	foreach ($fichaAlumno as $a) {
		echo "<br>    <tr>
                <th> Nombre:</th>
                <td>". $a-> nombre ."</td>
             </tr>
             <tr>
                <th>Apellido:</th>
                <td>". $a-> apellido ."</td>
            </tr>
            <tr>
                <th>Documento:</th>
                <td>". $a-> doc ."</td>
            </tr>
            <tr>
                <th>Fecha nacimiento:</th>
                <td>". $a-> fecha_nac ."</td>
            </tr>
            <tr>
                <th style='height: 150px;'>Observaciones:</th>
                <td style='word-wrap:break-word;'>". $a-> observaciones ."</td>
            </tr>";
            echo "</table>";
            echo "<div class='form-row text-center'>
                        <div class='col-12'>
                        <a class='btn btn-warning' href='editaralumno.php?alumno=".$a-> id_alumno."&escuela=".$a-> id_escuela."&grupo=".$a-> id_grupo."' role='button'>Editar</a>
                        <a class='btn btn-danger' href='eliminaralumno.php?alumno=".$a-> id_alumno."&escuela=".$a-> id_escuela."&grupo=".$a-> id_grupo."' role='button'>Eliminar</a>
                        </div>
                    </div>";
	}
		
?>
