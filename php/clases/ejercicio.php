<?php 

class Ejercicio{

    private $id;
    private $id_grupo;
    private $id_ejercicio;
    private $id_profesor;


    public function getId()
    {
        return $this->id;
    }

    public function setId($id)
    {
        $this->id= $id;

        return $this;
    }


    public function getIdEjercicio()
        {
            return $this->id;
        }

    public function setIdEjercicio($id_ejercicio)
    {
        $this->id_ejercicio= $id_ejercicio;

        return $this;
    }

    public function getIdProfesor()
        {
            return $this->id_profesor;
        }

    public function setIdProfesor($id_profesor)
    {
        $this->id_profesor= $id_profesor;

        return $this;
    }

    public function getIdGrupo()
        {
            return $this->id_grupo;
        }

    public function setIdGrupo($id_Grupo)
    {
        $this->id_grupo= $id_grupo;

        return $this;
    }

    public function getNombreEjercicio($id)

    {
            try {
                    $dsn = "mysql:host=localhost:3306;dbname=rodrigo1_iseiapp;charset=utf8";
                    $user = "rodrigo1_isei";
                    $password = "8{.bjQrvDc?z";
                    $dbh = new PDO($dsn, $user, $password);
                } catch (PDOException $e){
                    echo $e->getMessage();
                }
            $stmt = $dbh->prepare("SELECT * FROM Ejercicio WHERE id_ejercicio = :p1"); 
            $stmt->bindParam(':p1', $id);
            $stmt->execute();
            $ejercicio= $stmt->fetch(PDO::FETCH_OBJ);
            return $ejercicio->nombre;
    }
    public function getCalificacion($id, $valor)

        {
                try {
                        $dsn = "mysql:host=localhost:3306;dbname=rodrigo1_iseiapp;charset=utf8";
                        $user = "rodrigo1_isei";
                        $password = "8{.bjQrvDc?z";
                        $dbh = new PDO($dsn, $user, $password);
                    } catch (PDOException $e){
                        echo $e->getMessage();
                    }
                $stmt = $dbh->prepare("SELECT * FROM Escalacalif WHERE id_ejercicio = :p1"); 
                $stmt->bindParam(':p1', $id);
                $stmt->execute();
                $calificaciones= $stmt->fetchAll(PDO::FETCH_OBJ);
                $contador = 1;
                $limite = $stmt->rowCount();
                foreach ($calificaciones as $ca) {
                    if ($valor >= $ca-> valor_desde) {
                        if ($valor > $ca-> valor_hasta) {
                            if($contador == $limite){
                                return $ca-> calificacion;
                            }else{
                                $contador++;
                            }
                        }else{
                            return $ca-> calificacion;
                        }
                    }else{
                        return $ca-> calificacion;
                    }
                }
        }
        public function checkAsist($fecha, $id)

        {
                try {
                        $dsn = "mysql:host=localhost:3306;dbname=rodrigo1_iseiapp;charset=utf8";
                        $user = "rodrigo1_isei";
                        $password = "8{.bjQrvDc?z";
                        $dbh = new PDO($dsn, $user, $password);
                    } catch (PDOException $e){
                        echo $e->getMessage();
                    }
                $stmt = $dbh->prepare("SELECT * FROM Asistencia WHERE id_alumno = :p1 AND fecha = :p2"); 
                $stmt->bindParam(':p1', $id);
                $stmt->bindParam(':p2', $fecha);
                $stmt->execute();
                $asistencia= $stmt->fetch(PDO::FETCH_OBJ);

                if ($asistencia-> asistencia == 1) {
                    return true;
                }else{
                    return false;
                }

        }

}
?>