<?php
session_start();
    if (isset($_SESSION['user'])) {}else{header('location: login.php');} 
if (isset($_REQUEST['nuevo_grupo'])) {
  require_once realpath($_SERVER["DOCUMENT_ROOT"]) .'/php/acciones/add_group.php';
}
 ?>
<!DOCTYPE html>
<html lang="es">
<head>
    <?php require_once realpath($_SERVER["DOCUMENT_ROOT"]) . '/head.html';?>
    <style>
        .card{
            padding: 5px;
            margin-top: 15px;
            background-color: #232323;
        }
        </style>
    <title>ISEI App</title>
</head>
<body>
  <?php 
            require_once realpath($_SERVER["DOCUMENT_ROOT"]) . '/nav.html';  
           ?>
    <div class="container">
    <h1 class='text-center'>Nuevo grupo</h1>
        <form method="POST" action="">
        	<input type="hidden" name="escuela" value=<?php echo $_GET['escuela'];?>>
            <div class="form-group">
                <label for="nombre">Nombre</label>
                <input type="text" name="nombre" class="form-control" id="nombre">
            </div>
             <div class="form-group">
                <label for="nivel">Nivel</label>
                <select class="form-control" name="nivel" id="nivel">
                    <option value="Jardín">Jardín</option>
                    <option value="Primaria">Primaria</option>
                    <option value="Secundaria">Secundaria</option>
                </select>
            </div>
            <div class="form-group">
                <label for="turno">Turno</label>
                <select class="form-control" name="turno" id="turno">
                    <option value="Mañana">Mañana</option>
                    <option value="Tarde">Tarde</option>
                </select>
            </div>
            <div class="form-group">
                <label for="grado">Grado</label>
                <select class="form-control" name="grado" id="grado">
                <optgroup label="Jardin">
                    <option value="Jardín">Jardín</option>
                </optgroup>
                <optgroup label="Primaria">
                    <option value="Primero">Primero</option>
                    <option value="Segundo">Segundo</option>
                    <option value="Tercero">Tercero</option>
                    <option value="Cuarto">Cuarto</option>
                    <option value="Quinto">Quinto</option>
                    <option value="Sexto">Sexto</option>
                    <option value="Septimo">Septimo</option>
                </optgroup>
                <optgroup label="Secundaria">
                    <option value="Primer año">Primer año</option>
                    <option value="Segundo año">Segundo año</option>
                    <option value="Tercer año">Tercer año</option>
                    <option value="Cuarto año">Cuarto año</option>
                    <option value="Quinto año">Quinto año</option>
                </optgroup>
                </select> 
            </div>
            <div class="form-group">
                <label for="division">División</label>
                <input type="text" name="division" class="form-control" id="division">
            </div>
            <div class="form-row text-center">
                <div class="col-12">
                    <button type="submit" class="btn btn-success" name="nuevo_grupo">Guardar</button>
                </div>
            </div>
        </form>
    </div>
</body>
</html> 