<?php
require_once realpath($_SERVER["DOCUMENT_ROOT"]) .'/php/clases/grupo.php';
	session_start();
	$grupo =json_decode($_POST['grupo'], true);
	$id_prof = $_SESSION['user'];

    $a = new Grupo();
    $a->setIdProfesor($id_prof);
    $a->setIdEscuela($grupo[0]['escuela']);
	$a->setNombre($grupo[0]['nombre']);
    $a->setNivel($grupo[0]["nivel"]);
    $a->setTurno($grupo[0]["turno"]);
	$a->setGrado($grupo[0]["grado"]);
	$a->setDivision($grupo[0]["division"]);
	if($a->Agregar()){
		$respuesta = array('respuesta' => true);
	}else{
		$respuesta = array('respuesta' => false);
	}
	echo json_encode($respuesta);
?>