<?php
require_once realpath($_SERVER["DOCUMENT_ROOT"]) .'/php/clases/alumno.php';
if(isset($_POST["nombre"]) && $_POST["nombre"]!='')
{
    $a = new Alumno();
    $a->setId($_GET["alumno"]);
    $a->setId_escuela($_POST["escuela"]);
    $a->setGrupo($_GET["grupo"]);
	$a->setNombre($_POST["nombre"]);
	$a->setApellido($_POST["apellido"]);
	$a->setDoc($_POST["nro_doc"]);
    $a->setFecha_nac($_POST['fecha_nac']);
    $a->setNivel($_POST["nivel"]);
    $a->setTurno($_POST["turno"]);
	$a->setGrado($_POST["grado"]);
    $a->setDivision($_POST["division"]);
    $a->Editar();
}