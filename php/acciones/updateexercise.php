<?php
require_once realpath($_SERVER["DOCUMENT_ROOT"]) . '/php/db/DataBase.php';
$examen =json_decode($_POST['examen'], true);

$stmt = $dbh->prepare("UPDATE Notas SET nota=:p1 WHERE id = :p2");

foreach ($examen as $asists) {
	$params = array(":p1" => intval($asists['nota']), ":p2" =>intval($asists['id']));
	if ($stmt->execute($params)) {
		$respuesta = array('respuesta' => true);
	}else{
		$respuesta = array('respuesta' => false);
	}
}
$dbh=null;
echo json_encode($respuesta);

?>