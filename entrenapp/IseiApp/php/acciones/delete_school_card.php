<?php
require_once realpath($_SERVER["DOCUMENT_ROOT"]) . '/php/db/DataBase.php';
      
      $idescuela = $_GET['escuela']; 
			$stmt = $dbh->prepare("SELECT * FROM Escuela WHERE id_escuela = :p1");
			$stmt->bindParam(":p1", $idescuela);
			$stmt->execute();
      $escuela = $stmt->fetchAll(PDO::FETCH_OBJ);
            
            foreach ($escuela as $en) {
               echo "<div class='col-md-12'>
               <div class='card text-center'>
                   <div class='card-body'>
                       <i class='fas fa-trash fa-7x'></i> 
                       <hr>
                       <h3 class='card-text'>¿Estás seguro que deseas eliminar la escuela: ".$en-> nombre."?</h3>
                       <hr>
                       <div class='form-row text-center'>
                        <div class='col-12'>
                        <a class='btn btn-danger' href='php/acciones/delete_school.php?escuela=".$en-> id_escuela."' role='button'>Eliminar</a>
                        </div>
                    </div>
                   </div>
               </div>
           </div>";
            }
?>