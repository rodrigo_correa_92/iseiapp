<?php
session_start();
    if (isset($_SESSION['user'])) {}else{header('location: login.php');} 
if (isset($_REQUEST['nuevo_grupo'])) {
  require_once realpath($_SERVER["DOCUMENT_ROOT"]) .'/php/acciones/add_group.php';
}
 ?>
<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css" integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU" crossorigin="anonymous">
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
    <style>
        .card{
            padding: 5px;
            margin-top: 15px;
            background-color: #232323;
        }
        </style>
    <title>ISEI App</title>
</head>
<body>
    <nav class="navbar sticky-top navbar-expand-lg navbar-dark " style="background-color: #1a4768;">
        <a class="navbar-brand" href="escuela.php?escuela=<?php echo $_GET['escuela'];?> "><i class='fas fa-undo-alt'></i> Escuela</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarText" aria-controls="navbarText" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarText">
            <ul class="navbar-nav mr-auto nav-tabs">
            
            </ul>
            <ul class="navbar-nav">
                <li class="nav-item"><a class="nav-link" href="#"><i class="fas fa-user"></i>  Profesor</a></li>
            </ul>
        </div>
    </nav>
    <div class="container">
    <h1 class='text-center'>Nuevo grupo</h1>
        <form method="POST" action="">
        	<input type="hidden" name="escuela" value=<?php echo $_GET['escuela'];?>>
            <div class="form-group">
                <label for="nombre">Nombre</label>
                <input type="text" name="nombre" class="form-control" id="nombre">
            </div>
             <div class="form-group">
                <label for="nivel">Nivel</label>
                <select class="form-control" name="nivel" id="nivel">
                    <option value="Jardín">Jardín</option>
                    <option value="Primaria">Primaria</option>
                    <option value="Secundaria">Secundaria</option>
                </select>
            </div>
            <div class="form-group">
                <label for="turno">Turno</label>
                <select class="form-control" name="turno" id="turno">
                    <option value="Mañana">Mañana</option>
                    <option value="Tarde">Tarde</option>
                </select>
            </div>
            <div class="form-group">
                <label for="grado">Grado</label>
                <select class="form-control" name="grado" id="grado">
                <optgroup label="Jardin">
                    <option value="Jardín">Jardín</option>
                </optgroup>
                <optgroup label="Primaria">
                    <option value="Primero">Primero</option>
                    <option value="Segundo">Segundo</option>
                    <option value="Tercero">Tercero</option>
                    <option value="Cuarto">Cuarto</option>
                    <option value="Quinto">Quinto</option>
                    <option value="Sexto">Sexto</option>
                    <option value="Septimo">Septimo</option>
                </optgroup>
                <optgroup label="Secundaria">
                    <option value="Primer año">Primer año</option>
                    <option value="Segundo año">Segundo año</option>
                    <option value="Tercer año">Tercer año</option>
                    <option value="Cuarto año">Cuarto año</option>
                    <option value="Quinto año">Quinto año</option>
                </optgroup>
                </select> 
            </div>
            <div class="form-group">
                <label for="division">División</label>
                <input type="text" name="division" class="form-control" id="division">
            </div>
            <div class="form-row text-center">
                <div class="col-12">
                    <button type="submit" class="btn btn-success" name="nuevo_grupo">Guardar</button>
                </div>
            </div>
        </form>
    </div>
</body>
</html> 