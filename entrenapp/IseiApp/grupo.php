<?php session_start();
    if (isset($_SESSION['user'])) {}else{header('location: login.php');} ?>
<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css" integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU" crossorigin="anonymous">
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
 <script
  src="https://code.jquery.com/jquery-3.3.1.min.js"
  integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
  crossorigin="anonymous"></script>
    <style>
        .card{
            padding: 5px;
            margin-top: 15px;
            background-color: #232323;
        }
        </style>
    <title>ISEI App</title>
</head>
<body>
       <nav class="navbar sticky-top navbar-expand-lg navbar-dark " style="background-color: #1a4768;">
        <a class="navbar-brand" href="#">Entrenapp</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarText" aria-controls="navbarText" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarText">
            <ul class="navbar-nav mr-auto">
            </ul>
            <ul class="navbar-nav">
                <li class="nav-item"><i class="fas fa-user"></i>  <?php echo $_SESSION['nombre'] ?></li>
                <li class="nav-item"><a class="nav-link" href="logout.php"><i class="fas fa-power-off"></i>  Cerrar Sesión</a></li>
            </ul>
        </div>
    </nav>
    <div class="container">
        <?php 
            require_once realpath($_SERVER["DOCUMENT_ROOT"]) . '/php/acciones/group.php';  
           ?>
        </div>
    </div>
<!-- Modal -->
<div class="modal fade" id="nuevoAlumno" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Nuevo alumno</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <input type="hidden" name="grupo" id="grupo" value=<?php echo $_GET['grupo'];?>>
        <input type="hidden" name="escuela" id="escuela" value=<?php echo $_GET['escuela'];?>>
            <div class="form-group">
                <label for="nombre">Nombre</label>
                <input type="text" name="nombre" class="form-control" id="nombre" required>
            </div>
            <div class="form-group">
                <label for="apellido">Apellido</label>
                <input type="text" name="apellido" class="form-control" id="apellido" required>
            </div>
            <div class="form-group">
                <label for="documento">Documento</label>
                <input type="number" name="nro_doc" class="form-control" id="documento" required> 
            </div>
            <div class="form-group">
                <label for="fecha_nac">Fecha de nacimiento</label>
                <input type="date" name="fecha_nac" class="form-control" id="fecha_nac" required>
            </div>
            <div class="form-group">
                <label for="fecha_nac">Género</label>
                <select name="genero" id="genero" class="form-control">
                  <option value="1">Masculino</option>
                  <option value="2">Femenino</option>
                </select>
            </div>
        
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
        <button type="submit" class="btn btn-success" name="nuevo_alumno" id="nuevo_alumno">Guardar</button>
      </div>
    </div>
  </div>
</div>
</body>
<script>
if('serviceWorker' in navigator) {
  navigator.serviceWorker
           .register('/sw.js')
           .then(function() { console.log("Service Worker Registered"); });
}
var objAlumno = [];
$('#nuevo_alumno').click(function() {
            var escuela = $('#escuela').val();
            var grupo = $('#grupo').val();
            var nombre = $('#nombre').val();
            var apellido = $('#apellido').val();
            var documento = $('#documento').val();
            var fecha_nac = $('#fecha_nac').val();
            if (nombre == '' || apellido == '' || documento == '' || fecha_nac == '' ) {
              alert("completa todos los campos");
            }else{var genero = $('#genero option:selected').val();
            var arrayDatos = {'grupo':grupo,'escuela':escuela,'nombre':nombre, 'apellido':apellido, 'documento':documento, 'fecha_nac':fecha_nac, 'genero':genero};
            objAlumno.push(arrayDatos);
            var json = JSON.stringify(objAlumno);

        $('#nuevo_alumno').prop('disabled', true);
        
        $.ajax({
                        url: "/php/acciones/add_student.php",
                        type: "POST",
                        data:  {'alumno' : json},
                        dataType : 'json',
                        success: function(data)
                          {
                          if (data.respuesta == false) {alert("Carga incorrecta"); $('#nuevo_alumno').prop('disabled', false);}else{
                          alert("Carga correcta");location.reload();}
                          },
                        error: function() 
                        {
                          alert('Disculpe, existió un problema en el envío de datos, intente nuevamente más tarde');
                        }           
                  });
       objAlumno = [];}
            
    });
</script>
</html>