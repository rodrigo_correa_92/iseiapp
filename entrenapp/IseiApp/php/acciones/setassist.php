<?php
session_start();
$id_prof = $_SESSION['user'];
$asistencias =json_decode($_POST['asistencia'], true);
$id_grupo = $asistencias['0']['grupo'];

require_once realpath($_SERVER["DOCUMENT_ROOT"]) . '/php/db/DataBase.php';
$stmt = $dbh->prepare("INSERT INTO Registroasist (id_grupo, id_profesor, fecha) values(:p0,:p1,:p2)");
$params = array(":p0" => $id_grupo,":p1" => $id_prof,":p2" => date("Y-m-d H:i:s"));
$stmt->execute($params);
$id_asist = $dbh->lastInsertId();

$stmt = $dbh->prepare("INSERT INTO Asistencia (id_asistencia,id_escuela,id_alumno,id_grupo,fecha,asistencia) values(:p0,:p1,:p2,:p3,:p4,:p5)");

foreach ($asistencias as $asists) {
	$params = array(":p0" => intval($id_asist),":p1" => intval($asists['escuela']),":p2" => intval($asists['alumno']), ":p3" =>intval($asists['grupo']), ":p4" => date('Y-m-d'), ":p5" => intval($asists['asistencia']));
	if ($stmt->execute($params)) {
		$respuesta = array('respuesta' => true);
	}else{
		$respuesta = array('respuesta' => false);
	}
}
$dbh=null;
echo json_encode($respuesta);

?>