<?php session_start();
    if (isset($_SESSION['user'])) {}else{header('location: login.php');}
 ?>
<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css" integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU" crossorigin="anonymous">
    <script
  src="https://code.jquery.com/jquery-3.3.1.min.js"
  integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
  crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>

    <style>
        .card{
            padding: 5px;
            margin-top: 15px;
            background-color: #232323;
        }
        </style>
    <title>ISEI App</title>
</head>
<body>
    <nav class="navbar sticky-top navbar-expand-lg navbar-dark " style="background-color: #1a4768;">
        <a class="navbar-brand" href="grupo.php?grupo= <?php echo $_GET['grupo'].'&escuela='.$_GET['escuela'];?> "><i class='fas fa-undo-alt'></i> Grupo</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarText" aria-controls="navbarText" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarText">
            <ul class="navbar-nav mr-auto">
            </ul>
            <ul class="navbar-nav">
                <li class="nav-item"><a class="nav-link" href="#"><i class="fas fa-user"></i>  Profesor</a></li>
            </ul>
        </div>
    </nav>
    <div class="container">
    	<h1 style="text-align: center;">Actualizar asistencia</h1><br>
        	<h2 style="text-align: center;"><?php echo date('d/m/y') ?></h2>	
        <?php 
            require_once realpath($_SERVER["DOCUMENT_ROOT"]) . '/php/acciones/editassist.php';  
           ?>
        </div>
    </div>
</body>
<script type="text/javascript">
    var objAlumnos = [];
    $('#actualizarAsistencias').click(function() {
        $(".alumno").each(function( index ) {
            var id_alumno = $(this).attr('data-id');
            var id = $(this).attr('data-idAsist');
            var asistencia = $("#"+id_alumno).children().children('option:selected').val();
            var arrayDatos = {'id':id,'asistencia':asistencia};
            objAlumnos.push(arrayDatos);
        });
        var json = JSON.stringify(objAlumnos);
        
        $.ajax({
                        url: "/php/acciones/updateassist.php",
                        type: "POST",
                        data:  {'asistencia' : json},
                        dataType : 'json',
                        success: function(data)
                          {
                          if (data.respuesta == false) {alert("Carga incorrecta");}else{
                          alert("Carga correcta");
                         window.location.href = 'https://rodrigocorrea.me/grupo.php?grupo=<?php echo $_GET['grupo'].'&escuela='.$_GET['escuela'];?>';}
                          },
                        error: function() 
                        {
                          alert('Disculpe, existió un problema en el envío de datos, intente nuevamente más tarde');
                        }           
                  });
        objAlumnos = [];
    });

</script>
</html>