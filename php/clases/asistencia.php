<?php 

	class Asistencia{
            private $id_escuela;
            private $id_alumno;
            private $id_grupo;
            private $fecha;
            private $asistencia;



            /**
     * @return mixed
     */
    public function getIdEscuela()
    {
        return $this->id_escuela;
    }

    /**
     * @param mixed $id_escuela
     *
     * @return self
     */
    public function setIdEscuela($id_escuela)
    {
        $this->id_escuela = $id_escuela;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getIdAlumno()
    {
        return $this->id_alumno;
    }

    /**
     * @param mixed $id_alumno
     *
     * @return self
     */
    public function setIdAlumno($id_alumno)
    {
        $this->id_alumno = $id_alumno;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getIdGrupo()
    {
        return $this->id_grupo;
    }

    /**
     * @param mixed $id_grupo
     *
     * @return self
     */
    public function setIdGrupo($id_grupo)
    {
        $this->id_grupo = $id_grupo;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getFecha()
    {
        return $this->fecha;
    }

    /**
     * @param mixed $fecha
     *
     * @return self
     */
    public function setFecha($fecha)
    {
        $this->fecha = $fecha;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getAsistencia()
    {
        return $this->asistencia;
    }

    /**
     * @param mixed $asistencia
     *
     * @return self
     */
    public function setAsistencia($asistencia)
    {
        $this->asistencia = $asistencia;

        return $this;
    }
            
            

            public function Agregar(){
                require_once realpath($_SERVER["DOCUMENT_ROOT"]) . '/php/db/DataBase.php';
                $stmt = $dbh->prepare("INSERT INTO Asistencia (id_escuela,id_alumno,id_grupo,fecha,asistencia) values(:p1,:p2,:p3,:p4,:p5)");
                $params = array(":p1" => $this->id_escuela,":p2" => $this->id_alumno, ":p3" => $this->id_grupo, ":p4" => $this->fecha, ":p5" => $this->asistencia);
                $stmt->execute($params);
            }
            public function Editar()
        {
            require_once realpath($_SERVER["DOCUMENT_ROOT"]) . '/php/db/DataBase.php';
            $stmt = $dbh->prepare("UPDATE Escuela SET nro=:p1, nombre = :p2, telefono = :p3, direccion = :p4 WHERE id_escuela = :p5");
            $params = array(":p1" => $this->nro,":p2" => $this->nombre, ":p3" => $this->telefono, ":p4" => $this->direccion, ":p5" => $this->id_escuela);
            $stmt->execute($params);
            $dbh=null;
            header("location:escuela.php?escuela=". $this->id_escuela);
        }
        public function Eliminar()
        {
            require_once realpath($_SERVER["DOCUMENT_ROOT"]) . '/php/db/DataBase.php';
            $stmt = $dbh->prepare("UPDATE Escuela SET activo=:p1 WHERE id_escuela = :p2");
            $params = array(":p1" => 0, ":p2" => $this->id_escuela);
            $stmt->execute($params);
            $dbh=null;
            header("location:../../index.php");
        }

    
    
}