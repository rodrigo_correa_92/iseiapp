<?php 
session_start();
    if (isset($_SESSION['user'])) {}else{header('location: login.php');}
if (isset($_REQUEST['eliminar_alumno'])) {
  require_once realpath($_SERVER["DOCUMENT_ROOT"]) .'/php/acciones/delete_student.php';
}
 ?>
<!DOCTYPE html>
<html lang="es">
<head>
    <?php require_once realpath($_SERVER["DOCUMENT_ROOT"]) . '/head.html';?>
    <style>
        .card{
            padding: 5px;
            margin-top: 15px;
            background-color: #232323;
        }
        </style>
    <title>ISEI App</title>
</head>
<body>
       <nav class="navbar sticky-top navbar-expand-lg navbar-dark " style="background-color: #1a4768;">
        <a class="navbar-brand" href="#">Entrenapp</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarText" aria-controls="navbarText" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarText">
            <ul class="navbar-nav mr-auto">
            </ul>
            <ul class="navbar-nav">
                <li class="nav-item"><p><i class="fas fa-user"></i>  <?php echo $_SESSION['nombre'] ?></p></li>
                <li class="nav-item"><a class="nav-link" href="logout.php"><i class="fas fa-power-off"></i>  Cerrar Sesión</a></li>
            </ul>
        </div>
    </nav>
    <div class="container">
    <br>
    <h1 class='text-center'>Eliminar alumno</h1>
        <?php 
        require_once realpath($_SERVER["DOCUMENT_ROOT"]) .'/php/acciones/delete_student_card.php';
        ?>
    </div>
</body>
</html> 