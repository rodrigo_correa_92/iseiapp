<?php 

class Grupo{

    private $id_grupo;
    private $id_escuela;
	private $nombre;
    private $nivel;
    private $turno;
	private $grado;
	private $division;
    private $id_profesor;

    public function getIdProfesor()
        {
            return $this->id_profesor;
        }

    public function setIdProfesor($id_profesor)
    {
        $this->id_profesor= $id_profesor;

        return $this;
    }

    public function getIdGrupo()
    {
        return $this->id_grupo;
    }


    public function setIdGrupo($id_grupo)
    {
        $this->id_grupo = $id_grupo;

        return $this;
    }

    public function getIdEscuela()
    {
        return $this->id_escuela;
    }


    public function setIdEscuela($id_escuela)
    {
        $this->id_escuela = $id_escuela;

        return $this;
    }

    public function getNombre()
    {
        return $this->nombre;
    }


    public function setNombre($nombre)
    {
        $this->nombre = $nombre;

        return $this;
    }


    public function getNivel()
    {
        return $this->nivel;
    }


    public function setNivel($nivel)
    {
        $this->nivel = $nivel;

        return $this;
    }

    public function getTurno()
    {
        return $this->turno;
    }


    public function setTurno($turno)
    {
        $this->turno = $turno;

        return $this;
    }


    public function getGrado()
    {
        return $this->grado;
    }


    public function setGrado($grado)
    {
        $this->grado = $grado;

        return $this;
    }

    public function getDivision()
    {
        return $this->division;
    }

    public function setDivision($division)
    {
        $this->division = $division;

        return $this;
    }



    public function Agregar()
    {
        require_once realpath($_SERVER["DOCUMENT_ROOT"]) . '/php/db/DataBase.php';

        $stmt = $dbh->prepare("INSERT INTO Grupo (id_profesor,id_escuela,nombre,nivel,turno,grado,division,activo) values(:p1,:p2,:p3,:p4,:p5,:p6,:p7,:p8)");
        $params = array(":p1" => $this->id_profesor,":p2" => $this->id_escuela, ":p3" => $this->nombre, ":p4" => $this->nivel, ":p5" => $this->turno, ":p6" => $this->grado,":p7" => $this->division, ":p8" => 1);
            if($stmt->execute($params)){
                                return true;
                            }else{
                                return false;
                }
    }        
    public function Editar()
    {
        require_once realpath($_SERVER["DOCUMENT_ROOT"]) . '/php/db/DataBase.php';

        $stmt = $dbh->prepare("UPDATE Grupo SET nombre= :p1, nivel= :p2, turno= :p3, grado= :p4, division = :p5 WHERE id_grupo = :p6");
        $params = array(":p1" => $this->nombre, ":p2" => $this->nivel, ":p3" => $this->turno, ":p4" => $this->grado, ":p5" => $this->division, ":p6" => $this->id_grupo);
        if($stmt->execute($params)){
                                return true;
                            }else{
                                return false;
                }
    }
    public function Eliminar()
    {
       require_once realpath($_SERVER["DOCUMENT_ROOT"]) . '/php/db/DataBase.php';
            $stmt = $dbh->prepare("UPDATE Grupo SET activo=:p1 WHERE id_grupo = :p2");
            $params = array(":p1" => 0, ":p2" => $this->id_grupo);
            if($stmt->execute($params)){
                return true;
            }else{
                return false;
            }
    }
}
?>