<?php 
	require_once realpath($_SERVER["DOCUMENT_ROOT"]) . '/php/db/DataBase.php';
      		$email = $_POST['email']; 
      		$pass = $_POST['pass']; 

  $stmt = $dbh->prepare("SELECT * FROM Profesor WHERE email = :p1 AND pass = :p2");
  $stmt->bindParam(':p1', $email);
  $stmt->bindParam(':p2', $pass);
  $stmt->execute();
  

  if ($stmt->rowCount() > 0) {
    $users = $stmt->fetch(PDO::FETCH_ASSOC);
    ini_set("session.cookie_lifetime","7200");
    ini_set("session.gc_maxlifetime","7200");
      session_start();
      $_SESSION['user'] = $users['id_profesor'];
      $_SESSION['nombre'] = $users['nombre']." ". $users['apellido'];
      header('location: ../../index.php');
  }else{
    
    header('location: ../../login.php');
   }

 ?>
