<?php session_start();
    if (isset($_SESSION['user'])) {}else{header('location: login.php');}
    require_once realpath($_SERVER["DOCUMENT_ROOT"]) . '/php/clases/ejercicio.php';
 ?>
<!DOCTYPE html>
<html lang="es">
<head>
    <?php require_once realpath($_SERVER["DOCUMENT_ROOT"]) . '/head.html';?>
    <style>
        .card{
            padding: 5px;
            margin-top: 15px;
            background-color: #232323;
        }
        </style>
    <title>ISEI App</title>
</head>
<body>
       <nav class="navbar sticky-top navbar-expand-lg navbar-dark " style="background-color: #1a4768;">
        <a class="navbar-brand" href="#">Entrenapp</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarText" aria-controls="navbarText" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarText">
            <ul class="navbar-nav mr-auto">
            </ul>
            <ul class="navbar-nav">
                <li class="nav-item"><i class="fas fa-user"></i>  <?php echo $_SESSION['nombre'] ?></li>
                <li class="nav-item"><a class="nav-link" href="logout.php"><i class="fas fa-power-off"></i>  Cerrar Sesión</a></li>
            </ul>
        </div>
    </nav>
    <div class="container">
    		
        <?php 
            require_once realpath($_SERVER["DOCUMENT_ROOT"]) . '/php/acciones/editexercise.php';  
           ?>
        </div>
    </div>
</body>
<script type="text/javascript">
    var objAlumnos = [];
    $('#ActualizarNota').click(function(event) {
        event.preventDefault()
        $(".alumno").each(function( index ) {
             var id = $(this).attr('data-id');
            var id_alumno = $(this).attr('data-idAlumno');
            var nota = $("#"+id_alumno).children().val();
            var arrayDatos = {'alumno':id_alumno,'nota':nota, "id": id};
            objAlumnos.push(arrayDatos);
        });
        var json = JSON.stringify(objAlumnos);
        $('#ActualizarNota').prop('disabled', true);
        
        $.ajax({
                        url: "/php/acciones/updateexercise.php",
                        type: "POST",
                        data:  {'examen' : json},
                        dataType : 'json',
                        success: function(data)
                          {
                          if (data.respuesta == false) {alert("Carga incorrecta");location.reload();}else{
                          alert("Carga correcta");location.reload();}
                          },
                        error: function() 
                        {
                          alert('Disculpe, existió un problema en el envío de datos, intente nuevamente más tarde');
                        }           
                  });
        objAlumnos = [];
    });

</script>
</html>