
<!DOCTYPE html>
<html lang="es">
<head>
    <?php require_once realpath($_SERVER["DOCUMENT_ROOT"]) . '/head.html';?>
    <style>
        .card{
            padding: 5px;
            margin-top: 15px;
            background-color: #232323;
        }
        </style>
    <title>ISEI App</title>
</head>
<body>
       <nav class="navbar sticky-top navbar-expand-lg navbar-dark " style="background-color: #1a4768;">
        <a class="navbar-brand" href="#">Entrenapp</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarText" aria-controls="navbarText" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarText">
            <ul class="navbar-nav mr-auto">
            </ul>
            <ul class="navbar-nav">
                <li class="nav-item"><p><i class="fas fa-user"></i>  <?php echo $_SESSION['nombre'] ?></p></li>
                <li class="nav-item"><a class="nav-link" href="logout.php"><i class="fas fa-power-off"></i>  Cerrar Sesión</a></li>
            </ul>
        </div>
    </nav>
    <div class="container">
    <h1 class='text-center'>Nueva escuela</h1>
        <form method="POST" action="/php/acciones/add_school.php">
            <div class="form-group">
                <label for="nro">Número</label>
                <input type="number" name="nro" class="form-control" id="nro">
            </div>
            <div class="form-group">
                <label for="nombre">Nombre</label>
                <input type="text" name="nombre" class="form-control" id="nombre">
            </div>
            <div class="form-group">
                <label for="telefono">Telefono</label>
                <input type="number" name="telefono" class="form-control" id="telefono"> 
            </div>
            <div class="form-group">
                <label for="direccion">Dirección</label>
                <input type="text" name="direccion" class="form-control" id="direccion">
            </div>
            <div class="form-row text-center">
                <div class="col-12">
                    <button type="submit" class="btn btn-success" name="nueva_escuela">Guardar</button>
                </div>
            </div>
        </form>
    </div>
</body>
</html> 